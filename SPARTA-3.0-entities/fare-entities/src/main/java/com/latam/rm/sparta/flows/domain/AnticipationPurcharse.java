/**
 * 
 */
package com.latam.rm.sparta.flows.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author ocuadran
 *
 */
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Data
public class AnticipationPurcharse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotNull(message="{domain.anticipationpurcharse.code.notnull}")
	private String code;

	@NotNull(message="{domain.anticipationpurcharse.user.notnull}")
	private String user;
	
}
