package com.latam.rm.sparta.flows.domain;

import java.io.Serializable;

import javax.validation.constraints.AssertTrue;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Data
public class BlackOutRestriction implements Serializable {
	private static final long serialVersionUID = 1L;

	private PeriodDates origin;

	private PeriodDates destination;

	@AssertTrue(message = "{domain.blackoutrestriction.isoriginnotnull}")
	public boolean isOriginNotNull() {
		return (origin.getStart() != null && origin.getTo() != null);
	}

	@AssertTrue(message = "{domain.blackoutrestriction.isdestinationnotnull}")
	public boolean isDestinationNotNull() {
		return (destination.getStart() != null && destination.getTo() != null);
	}

}
