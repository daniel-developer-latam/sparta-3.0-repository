package com.latam.rm.sparta.flows.domain;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by Sebastián Gómez on 05/06/2017.
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false)
public abstract class Fare {
	
	/**
	 * Para contabilizar el numero de la fila que lee
	 */
	private Integer rowNumGrid;

	@NotNull(message = "{domain.fare.action.notnull}")
	private String action;

	@NotNull(message = "{domain.fare.carrierlist.notnull}")
	@Size(min = 1, message = "{domain.fare.carrierlist.sizemin}" )
	private List<String> carrierList;

	@NotNull(message = "{domain.fare.origin.notnull}")
	@Size (min = 3, max = 3, message="{domain.fare.origin.size}")
	private String origin;
	
	@NotNull(message = "{domain.fare.destination.notnull}")
	@Size (min = 3, max = 3, message="{domain.fare.destination.size}")
	private String destination;
	
	@NotNull(message = "{domain.fare.classcode.notnull}")
	@Size(max=1, message = "{domain.fare.classcode.size}" )
	private String classCode;

	@NotNull(message = "{domain.fare.farebasis.notnull}")
	@Size(max=8, message = "{domain.fare.farebasis.size}" )
	private String farebasis;
	
	@NotNull(message = "{domain.fare.fareamountfacial.notnull}")
	@DecimalMin(value = "0.0", message = "{domain.fare.fareamountfacial.min}" )
	@DecimalMax(value = "999999999", message = "{domain.fare.fareamountfacial.max}" )
	private Double fareAmountFacial;

	@NotNull(message = "{domain.fare.currency.notnull}")
	@Size(max = 3, message ="{domain.fare.currency.size}")
	private String currency;
	
	private Integer tariff;
	
	private Integer routingPR;
	
	public String carriersToString() {
		if (this.carrierList != null) {
			return this.carrierList.stream().map(i -> i.toString()).collect(Collectors.joining(", "));
		}
		return null;
	}

}
