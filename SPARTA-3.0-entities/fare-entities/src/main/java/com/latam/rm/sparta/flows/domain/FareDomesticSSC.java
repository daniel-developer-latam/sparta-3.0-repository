package com.latam.rm.sparta.flows.domain;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Fare Strucure Domestic
 * 
 * @author ocuadran
 *
 */

@ToString
@EqualsAndHashCode(callSuper = false)
@Data
public class FareDomesticSSC extends Fare implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FareDomesticSSC() {
		super();
	}
	
	@NotNull(message = "{domain.faredomesticssc.cabin.notnull}")
	private String cabin;

	@NotNull(message = "{domain.faredomesticssc.product.notnull}")
	private Product product;

	@Size(max=10, message = "{domain.faredomesticssc.dowmatch.size}" )
	private String dowMatch;

	@NotNull(message = "{domain.faredomesticssc.family.notnull}")
	private String family;

	@NotNull(message = "{domain.faredomesticssc.anticipationpurcharse.notnull}")
	private AnticipationPurcharse anticipationPurcharse;

	@NotNull(message = "{domain.faredomesticssc.minimunstay.notnull}")
	private MinimumStay minimunStay;

	@NotNull(message = "{domain.faredomesticssc.surcharge.notnull}")
	private Surcharge surcharge;

	private String typeTrip; // Atributo OW/RT

	private SalesRestriction salesRestriction;

	private TravelRestriction travelRestiction;
	
	private String blackOut;
	
	private String salesRestrictionPos;

	private List<List<BlackOutRestriction>> blackouts;

	private List<List<SalesRestrictionPost>> salesRestrictionsPost;
	
	private FootNote footNotesRM;
	
	private FootNote footNotesPR;

	private RoutingFl routingFL;
}