/**
 * 
 */
package com.latam.rm.sparta.flows.domain;

import java.io.Serializable;

import javax.validation.constraints.Pattern;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author ocuadran
 *
 */
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Data
public class FootNote implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Pattern(regexp = "^[\\p{Alnum}]{2}$", message="{domain.footnote.value.pattern}")
	private String value;

	private String carrier;
}
