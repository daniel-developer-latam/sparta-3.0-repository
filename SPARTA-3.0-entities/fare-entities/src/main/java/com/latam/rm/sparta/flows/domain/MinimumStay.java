/**
 * 
 */
package com.latam.rm.sparta.flows.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author ocuadran
 *
 */

@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Data
public class MinimumStay implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@NotNull(message = "{domain.minimumstay.code.notnull}")
	private String code;

	@NotNull(message = "{domain.minimumstay.user.notnull}")
	private String user;
	
}
