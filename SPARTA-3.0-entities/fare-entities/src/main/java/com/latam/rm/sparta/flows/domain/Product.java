package com.latam.rm.sparta.flows.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * Representa un codigo relacionado con los productos
 * 
 * @author dcarvaja
 * 
 */

@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Data
public class Product implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Corresponde al campo SEASON/DOW/TOD/FL
	 */
	@NotNull(message = "{domain.product.code.notnull}")
	private String code;

	/**
	 * Corresponde al campo Definción código
	 */
	@NotNull(message = "{domain.product.description.notnull}")
	private String description;

}
