package com.latam.rm.sparta.flows.domain;

import java.io.Serializable;

import javax.validation.constraints.AssertTrue;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Data
public class SalesRestriction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PeriodDates periodDates;
	
	@AssertTrue(message = "{domain.salesrestriction.isperiodatesnotnull}")
	public boolean isPeriodDatesNotNull() {
		return (periodDates.getStart() != null && periodDates.getTo() != null);
	}
	

}
