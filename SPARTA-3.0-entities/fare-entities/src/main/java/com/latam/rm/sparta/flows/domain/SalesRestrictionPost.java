package com.latam.rm.sparta.flows.domain;

import java.io.Serializable;

import javax.validation.constraints.Pattern;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Data
public class SalesRestrictionPost implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Pattern(regexp = "^[\\p{Alpha}]{2}$", message="{domain.salesrestrictionpost.only.pattern}")
	private String only;
	
	@Pattern(regexp = "^[\\p{Alpha}]{2}$", message="{domain.salesrestrictionpost.exclude.pattern}")
	private String exclude;
	
	@Pattern(regexp = "^[\\p{Alpha}]{2}$", message="{domain.salesrestrictionpost.include.pattern}")
	private String include;


}
