package com.latam.rm.sparta.flows.domain;

import java.io.Serializable;

import lombok.Data;

@Data
public class Surcharge implements Serializable {
		
	private static final long serialVersionUID = 1L;
	
	private String code;
		
	private String user;

}
