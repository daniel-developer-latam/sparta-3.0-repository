package com.latam.rm.sparta.flows.domain;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Data
public class TravelRestriction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PeriodDates periodDates;
	
	private Date completeDate;
	

}
