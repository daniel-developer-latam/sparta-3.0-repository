package com.latam.rm.sparta.flows.command;

import lombok.Data;

@Data
public class FareSSCValidateCommand extends FareValidateCommand {
	 	 
	private static final long serialVersionUID = 1L;
	
}
