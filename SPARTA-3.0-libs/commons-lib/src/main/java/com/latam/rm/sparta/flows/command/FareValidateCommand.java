package com.latam.rm.sparta.flows.command;

import java.io.Serializable;

import lombok.Data;

@Data
public abstract class FareValidateCommand implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String fareId;
	//
	private String action;
	private String carrier;
	private String origin;
	private String destination;
	private String classCode;
	// SEASON/DOW/TOD/FL
	private String productCode;
	// Definicion codigo
	private String productDescription;

	private String dowMatch;
	private String family;

	private String apCode;
	private String apUser;

	private String mnCode;
	private String mnUser;

	private String surchargeCode;
	private String surchargeUser;

	private String farebasis;
	// Atributo OW/RT
	private Integer typeTrip;

	private Double fareAmountFacial;
	private String currency;

	// Sales Restriction
	private String sRestrictionStart;
	private String sRestrictionTo;
	// Travel Restriction
	private String from;
	private String to;
	private String comp;

	// TODO: Colocar la lista de BO y SR

	private String tariff;
	private String routingPR;
	private String cabin;

	private String rountingFLcode;
	private String rountingFLdescription;

}
