package com.latam.rm.sparta.flows.command;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class SalesRestrictionValidateCommand implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String Id;
	private String mandatory;
	//
	private String only;
	private String exclude;
	private String include;
		
}
