package com.latam.rm.sparta.flows.dto;

import lombok.Data;

@Data
public class GatewayContextDto {
	
	String config;

}
