package com.latam.rm.sparta.flows.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class MinimumStayDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String code;

	private String user;

	private String config;

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(final String code) {
		this.code = (code == null) ? "" : code;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(final String user) {
		this.user = (user == null) ? "" : user;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(final String config) {
		this.config = (config == null) ? "" : config;
	}

}
