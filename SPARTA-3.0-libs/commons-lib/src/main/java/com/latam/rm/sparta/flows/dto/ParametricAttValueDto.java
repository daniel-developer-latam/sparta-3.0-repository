package com.latam.rm.sparta.flows.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ParametricAttValueDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	 
    private String paattyId;

    private String keyValue;

    private String teconfId;

    private String secondValue;

    private String fifthValue;

    private String fourthValue;

    private String thirdValue;
 
}