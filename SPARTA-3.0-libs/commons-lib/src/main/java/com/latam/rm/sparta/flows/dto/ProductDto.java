package com.latam.rm.sparta.flows.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ProductDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String code;

	private String description;

}
