/**
 * 
 */
package com.latam.rm.sparta.flows.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class RoutingFlightDto implements Serializable {
	
	static final long serialVersionUID = 1L;
	
	private String code;
	
	private String description;
	
}
