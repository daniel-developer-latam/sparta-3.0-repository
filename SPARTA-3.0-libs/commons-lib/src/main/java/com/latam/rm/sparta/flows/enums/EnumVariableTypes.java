package com.latam.rm.sparta.flows.enums;

public enum EnumVariableTypes {
	
	ACTION("ACTION"),
	CXR("CXR"),
	CABIN("Cabin"),
	CLASS("Class"),
	SEASON("Season"),
	DOW("DOW"),
	SURCHARGE("Surcharge"),
	SEGMENT("Segment"),
	FAMILY("Family"),
	ADVP("ADVP (antecedencia)"),
	ODPOS("ODPOS"),
	AP("AP"),
	UP("UP"),
	RB("RB"),
	SL("SL"),
	TKT("TKT"),
	MN("MN"),
	OWRT("OW/RT"),
	MATCHTYPE("MATCHTYPE"),
	CXROAL("Carrier OAL"),
	MARKET("Market"),
	SEASON_DOW_TOD_FL("SEASON/DOW/TOD/FL"),
	TIKETING_FAREBOX("Tiketing - Fare Box"),
	AMOUNT("Amount"),
	PCC_IATA_OFID("PCC/IATA/OF ID"),
	GDS("GDS"),
	VOO("Voo"),
	VIAGEM("Viagem"),
	VENDA("Venda"),
	BLACKOUT("BlackOut"),
	TYPE("Type"),
	FAREBASIS_CONVERTION("Farebasis parameter convertion (USD)"),
	MARKET_TYPE_DAY_TIME("Market Type/Day Time"),
	MINIMUM_STAY("Minimum Stay"),
	CLASS_BUNDLE("Class Bundle"),
	RTOW_VS_BRLUSD("RTOW vs BRL/USD"),
	MINIMUM_AMOUNT("Minimum amount"),
	COMISSIONTYPE("ComissionType"),
	FBRMAXPER ("FBRmax%"),
	FBRMAXAMO ("FBRmaxAmount"),
	PRIVADAS("Privadas"),
	ROUTING_FL("Routing FL"),
	HIGHRATE("HIGHRATE");
	
	private final String id;

	private EnumVariableTypes(String value) {
		this.id = value;
	}

	public String getId() {
		return id;
	}
	
}
