package com.latam.rm.sparta.flows.enums;

import java.util.HashMap;

public enum ItemsInterExcelColumsEnum {
	ACTION(0, "ACTION"),
	CARRIER(1, "CXR"),	
	ORIGIN(2, "Origin"),
	DESTINATION(3, "Destination"),
	CABIN(4, "Cabin"),
	CLASS(5, "Class"),
	SEASON(6, "Season"),
	DOW(7, "DOW"),	
	DOW_MATCH(8, "DOW Match"),
	FAMILY(9, "Family"),	
	USER_AP(10, "User AP"),
	COD_AP(11, "Cód AP"),
	USER_MN(12, "User MN"),
	COD_MN(13, "Cód MN"),
	ORIGIN_COUNTRY(14, "Pais Origen"),
	SURCHARGE(15, "Surcharge"),
	FAREBASIS(16, "Farebasis"),
	OWRT(17, "OW/RT");
	//TODO Llenar los campos que faltan
	
	private final Integer id;
	private final String name;

	private ItemsInterExcelColumsEnum(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public static HashMap<Integer, String> getHashMapValues() {
		HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
		for (ItemsInterExcelColumsEnum e : ItemsInterExcelColumsEnum.values()) {
			hashMap.put(e.getId(), e.getName());
		}
		return hashMap;
	}	
}
