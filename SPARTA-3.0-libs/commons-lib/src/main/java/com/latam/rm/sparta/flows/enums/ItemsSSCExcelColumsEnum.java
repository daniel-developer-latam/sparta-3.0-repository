package com.latam.rm.sparta.flows.enums;

import java.util.HashMap;

public enum ItemsSSCExcelColumsEnum {	
	ACTION(0, "ACTION"),
	CXR(1, "CXR"),	
	ORIGIN(3, "Origin"),
	DESTINATION(4, "Destination"),
	CABIN(5, "Cabin"),
	CLASS(6, "Class"),
	SEASON_DOW_TOD_FL(7, "SEASON/DOW/TOD/FL"),
	DEFINICION_CODIGO(8, "Definición código"),
	DOW_MATCH(9, "DOW Match"),
	FAMILY(10, "Family"),	
	USER_AP(11, "User AP"),
	COD_AP(12, "Cód AP"),
	USER_MN(13, "User MN"),
	COD_MN(14, "Cód MN"),
	CODE_SURCHARGE(15, "Code"),
	USER_SURCHARGE(16, "User"),	
	OWRT(17, "OW/RT");
	//TODO Llenar los campos que faltan
	
	private final Integer id;
	private final String name;

	private ItemsSSCExcelColumsEnum(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public static HashMap<Integer, String> getHashMapValues() {
		HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
		for (ItemsSSCExcelColumsEnum e : ItemsSSCExcelColumsEnum.values()) {
			hashMap.put(e.getId(), e.getName());
		}
		return hashMap;
	}	
}
