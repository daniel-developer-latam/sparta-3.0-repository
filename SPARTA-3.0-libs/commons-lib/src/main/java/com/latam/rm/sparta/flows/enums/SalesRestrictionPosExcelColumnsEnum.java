package com.latam.rm.sparta.flows.enums;

import java.util.HashMap;

public enum SalesRestrictionPosExcelColumnsEnum {
	SR_ID(2, "SR"),
	SR_PAIS(0, "SR (Pais)"),
    ONLY(0, "Only" ),
    EXCLUDE(1, "Exclude"),
    INCLUDE(2, "Include");  

    private final Integer id;
    private final String name; 

	private SalesRestrictionPosExcelColumnsEnum(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
    public static HashMap<Integer, String> getHashMapValues() {
        HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
        for (SalesRestrictionPosExcelColumnsEnum e : SalesRestrictionPosExcelColumnsEnum.values()) {
            hashMap.put(e.getId(), e.getName());
        }
        return hashMap;
    }
}
