package com.latam.rm.sparta.flows.event;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnticipationPurcharseIntegrityValidatedEvent {
	
	private String id;
	
	private List<String> message; 
	
}
