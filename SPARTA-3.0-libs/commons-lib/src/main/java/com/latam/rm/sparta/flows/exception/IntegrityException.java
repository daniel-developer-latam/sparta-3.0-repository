package com.latam.rm.sparta.flows.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class IntegrityException extends Exception {

	private static final long serialVersionUID = 1L;

    public IntegrityException() {
        
    }

    /**
 * @param message Mensage.
 */
    public IntegrityException(String message) {
        super(message);
    }

    /**
 * @param cause Causa.
 */
    public IntegrityException(Throwable cause) {
        super(cause);
    }

    /**
 * @param message Mensage.
 * @param cause Causa.
 */
    public IntegrityException(String message, Throwable cause) {
        super(message, cause);
    }
}