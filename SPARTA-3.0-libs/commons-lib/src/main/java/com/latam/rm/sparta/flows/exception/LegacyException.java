package com.latam.rm.sparta.flows.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class LegacyException extends Exception {

	private static final long serialVersionUID = 1L;

	public LegacyException() {
	}

	public LegacyException(String message) {
		super(message);
	}

	public LegacyException(Throwable cause) {
		super(cause);
	}

	public LegacyException(String message, Throwable cause) {
		super(message, cause);
	}
}