package com.latam.rm.sparta.flows.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Clase encargada de validaciones.
 * 
 * @author Daniel Carvajal.
 */
public final class UtilValidate {

    private UtilValidate() {
    }

    /**
     * Validaciones de datos.
     * 
     * @param o
     * @return
     */
    public static boolean isList(Object o) {
        return o.getClass().isInstance(List.class);
    }

    
    /**
     * validaci�n de vacio, string.
     * 
     * @param o
     * @return
     */
    public static boolean isEmpty(String str) {
    	return StringUtils.isEmpty(str);
    }

    /**
     * validaci�n de vacio numero.
     * 
     * @param number
     * @return
     */
    public static boolean isEmpty(int number) {
        boolean isEmpty = false;
        if (number == 0) {
            isEmpty = true;
        }
        return isEmpty;
    }

    /**
     * validacion de nulo en objeto.
     * 
     * @param o
     * @return
     */
    public static boolean isEmpty(Object o) {
        boolean isEmpty = false;
        if (o == null) {
            isEmpty = true;
        }
        return isEmpty;
    }

    /**
     * validacion de un BigInteger vacio
     * 
     * @param o
     * @return
     */
    public static boolean isEmpty(BigInteger bignumber) {
        boolean isEmpty = false;
        if (bignumber == null || bignumber.equals(BigInteger.ZERO)) {
            isEmpty = true;
        }
        return isEmpty;
    }

    /**
     * validacion de un BigDecimal vacio
     * 
     * @param o
     * @return
     */
    public static boolean isEmpty(BigDecimal bigDecimal) {
        boolean isEmpty = false;
        BigDecimal bigDecimalZero = new BigDecimal("0");
        if (bigDecimal == null || bigDecimal.equals(bigDecimalZero)) {
            isEmpty = true;
        }
        return isEmpty;
    }

    /**
     * validacion de si lista es vacia.
     * 
     * @param list
     * @return
     */
    public static boolean isEmpty(List list) {
        boolean isEmpty = false;
        if (list == null || list.size() == 0) {
            isEmpty = true;
        }
        return isEmpty;
    }

    /**
     * validacion de si lista es vacia.
     * 
     * @param list
     * @return
     */
    public static boolean isEmpty(Object[] list) {
        boolean isEmpty = false;
        if (list == null || list.length == 0) {
            isEmpty = true;
        }
        return isEmpty;
    }

     /**
     * valida si string contiene secuencia.
     * 
     * @param strContainer
     * @param charSecuence
     * @return
     */
    public static boolean contains(String strContainer, String charSecuence) {
        boolean isContains = false;
        int index = strContainer.indexOf(charSecuence);
        if (index > -1) {
            isContains = true;
        }
        return isContains;
    }

   
    public static boolean checkDateFormat(String strDate, String strFormat) {
        boolean isDateFormatOk = true;
        SimpleDateFormat dateFormat = new SimpleDateFormat(strFormat);
        try {
            dateFormat.parse(strDate);
        } catch (ParseException e) {
            isDateFormatOk = false;
        }
        return isDateFormatOk;
    }
}
