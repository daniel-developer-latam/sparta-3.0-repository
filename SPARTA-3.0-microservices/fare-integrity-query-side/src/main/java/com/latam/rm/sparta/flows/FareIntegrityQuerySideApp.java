package com.latam.rm.sparta.flows;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FareIntegrityQuerySideApp {

	private static Class<?>[] AppConfigurations = 
						{ FareIntegrityQuerySideApp.class, CouchbaseCacheConfig.class };

	public static void main(String args[]) {
		SpringApplication.run(AppConfigurations, args);
	}
}
