package com.latam.rm.sparta.flows.exception;

public class MinimumStayException extends Exception {

    private static final long serialVersionUID = 1L;

    public MinimumStayException() {        
    }

    public MinimumStayException(String message) {
        super(message);
    }

    public MinimumStayException(Throwable cause) {
        super(cause);
    }

    public MinimumStayException(String message, Throwable cause) {
        super(message, cause);
    }
}