package com.latam.rm.sparta.flows.persistence.mybatis;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.latam.rm.sparta.flows.dto.MinimumStayDto;

@Mapper
public interface MinimumStayMapper {
	
	List<MinimumStayDto> getAll();
	
	List<MinimumStayDto> getAllByConfig(@Param("config") String config);
	
	MinimumStayDto getByUserAndConfig(@Param("user") String user, @Param("config") String config);
		
}