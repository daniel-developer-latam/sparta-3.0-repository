package com.latam.rm.sparta.flows.repository;

import java.util.List;

import com.latam.rm.sparta.flows.dto.MinimumStayDto;

public interface MinimumStayRepository {

	List<MinimumStayDto> getAll();

	List<MinimumStayDto> getAllByConfig(String config);

	MinimumStayDto getByUserAndConfig(String user, String config);
}
