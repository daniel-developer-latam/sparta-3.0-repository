package com.latam.rm.sparta.flows.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.latam.rm.sparta.flows.CouchbaseCacheConfig;
import com.latam.rm.sparta.flows.dto.MinimumStayDto;
import com.latam.rm.sparta.flows.persistence.mybatis.MinimumStayMapper;
import com.latam.rm.sparta.flows.repository.MinimumStayRepository;

@Repository
public class MinimumStayRepositoryImpl implements MinimumStayRepository {
	
	@Autowired
	MinimumStayMapper mapper;

	
	@Cacheable(value = CouchbaseCacheConfig.MINIMUMSTAY_CACHE)
	public List<MinimumStayDto> getAll() {
		return mapper.getAll();
	}

	@Cacheable(value = CouchbaseCacheConfig.MINIMUMSTAY_CACHE)	
	public List<MinimumStayDto> getAllByConfig(String config) {
		return mapper.getAllByConfig(config);
	}

	@Cacheable(value = CouchbaseCacheConfig.MINIMUMSTAY_CACHE)
	public MinimumStayDto getByUserAndConfig(String user, String config) {
		return mapper.getByUserAndConfig(user, config);
	}	
}
