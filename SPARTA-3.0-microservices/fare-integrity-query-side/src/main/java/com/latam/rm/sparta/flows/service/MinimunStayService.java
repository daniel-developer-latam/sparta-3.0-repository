package com.latam.rm.sparta.flows.service;

import com.latam.rm.sparta.flows.dto.MinimumStayDto;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MinimunStayService {

	Flux<MinimumStayDto> getAll(); 

	Flux<MinimumStayDto> getAllByConfig(String config);

	Mono<MinimumStayDto> getByUserAndConfig(String user, String config);
}
