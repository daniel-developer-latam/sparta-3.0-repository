package com.latam.rm.sparta.flows.service.impl;

import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.latam.rm.sparta.flows.dto.MinimumStayDto;
import com.latam.rm.sparta.flows.exception.MinimumStayException;
import com.latam.rm.sparta.flows.repository.MinimumStayRepository;
import com.latam.rm.sparta.flows.service.MinimunStayService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MinimunStayServiceImpl implements MinimunStayService {

	@Autowired
	MinimumStayRepository repository;

	
	public Flux<MinimumStayDto> getAll() {
		return Flux.fromIterable(repository.getAll());
	}

	public Flux<MinimumStayDto> getAllByConfig(final String config) {
		return 
			Flux.fromIterable(repository.getAll())
				.filter(new Predicate<MinimumStayDto>() {

					public boolean test(MinimumStayDto minimumStayDto) {
							return minimumStayDto.getConfig().equals(config.trim());
					}
				}); 
	}

	public Mono<MinimumStayDto> getByUserAndConfig(final String user, final String config) {
		return 
			Flux.fromIterable(repository.getAll())
				.filter(new Predicate<MinimumStayDto>() {

					public boolean test(MinimumStayDto minimumStayDto) {
							boolean isEquals = false;
								if(minimumStayDto.getUser().equals(user.trim()) &&
										minimumStayDto.getConfig().equals(config.trim())) {
									isEquals = true;
								}
							return isEquals; 
					}
				}).singleOrEmpty(); 

	}

}
