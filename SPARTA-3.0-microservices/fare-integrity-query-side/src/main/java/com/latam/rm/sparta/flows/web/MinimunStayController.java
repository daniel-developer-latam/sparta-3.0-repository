package com.latam.rm.sparta.flows.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.latam.rm.sparta.flows.dto.MinimumStayDto;
import com.latam.rm.sparta.flows.service.MinimunStayService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/minimunStay")
public class MinimunStayController {

	@Autowired
	MinimunStayService service;

	@RequestMapping(method = RequestMethod.GET, path = "/getAll")
	public @ResponseBody Flux<MinimumStayDto> getAll() {
		return service.getAll();
	}

	@RequestMapping(method = RequestMethod.GET, path = "/getAllByConfig")
	public @ResponseBody Flux<MinimumStayDto> getAllByConfig(@RequestParam String config)  {
		return service.getAllByConfig(config);

	}

	@RequestMapping(method = RequestMethod.GET, path = "/getByUserAndConfig")
	public @ResponseBody Mono<MinimumStayDto> getByUserAndConfig(
			@RequestParam String user, @RequestParam String config) {
		return service.getByUserAndConfig(user, config);
	}
}
