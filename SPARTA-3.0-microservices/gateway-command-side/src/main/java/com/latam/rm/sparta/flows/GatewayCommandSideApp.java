package com.latam.rm.sparta.flows;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatewayCommandSideApp {

	public static void main(String args[]) {
		SpringApplication.run(GatewayCommandSideConfig.class, args);		
	}
}
