package com.latam.rm.sparta.flows.converter;

import java.util.stream.Stream;

import com.latam.rm.sparta.flows.dto.GatewayContextDto;

import reactor.core.publisher.Flux;

public interface CommandConverter {

	Flux<?> convertCsvToCommand(Stream<String> rows, GatewayContextDto gatewayContextDto);

}
