package com.latam.rm.sparta.flows.converter;

import com.latam.rm.sparta.flows.converter.impl.SalesRestrictionPosCommandConverter;

public enum CommandConverterFactoryEnum {
	
	SALES_RESTRICTION_DOMESTIC_SSC("SALES-RESTRICTION-DOMESTIC-SSC", SalesRestrictionPosCommandConverter.class);
		
	private String idConverter;
	private Class<? extends CommandConverter> clazz;
	
	private CommandConverterFactoryEnum(String idConverter, Class<? extends CommandConverter> clazz) {
		this.idConverter = idConverter;
		this.clazz = clazz;
	}	
	
	public String getIdConverter() {
		return idConverter;
	}

	public Class<? extends CommandConverter> getClazz() {
		return clazz;
	}
	
	public static CommandConverter getConverter(String idConverter) throws InstantiationException, IllegalAccessException {
		CommandConverter converter = null;
		for (CommandConverterFactoryEnum e: CommandConverterFactoryEnum.values()) {
			if(e.getIdConverter().equals(idConverter)){
				converter = e.getClazz().newInstance();				
			}
		}
		return converter;
	}
}
