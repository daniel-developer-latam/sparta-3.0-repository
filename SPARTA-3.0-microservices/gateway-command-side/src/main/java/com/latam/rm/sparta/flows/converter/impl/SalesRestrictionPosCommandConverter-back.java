package com.latam.rm.sparta.flows.converter.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.latam.rm.sparta.flows.command.SalesRestrictionSSCValidateCommand;
import com.latam.rm.sparta.flows.converter.CommandConverter;
import com.latam.rm.sparta.flows.dto.GatewayContextDto;
import com.latam.rm.sparta.flows.enums.SalesRestrictionPosExcelColumnsEnum;

public class SalesRestrictionPosCommandConverter implements CommandConverter {
	
	private String salesRestrictionId;
	
	public static final String STATUS_OK = "STATUS_OK";
	public static final String STATUS_NOK = "STATUS_NOK";
	
	
	@Override
	public List<SalesRestrictionSSCValidateCommand> convertToCommand(Sheet sheet, GatewayContextDto gatewayContextDto) {
		return 
			StreamSupport.stream(sheet.spliterator(), false)
				.filter(row ->  isRowNotEmpty(row))
					.map(row -> convertRowToCommand(row))
						.filter(command -> isCommandNotEmpty(command))
							.collect(Collectors.toList());
	}
		
	protected SalesRestrictionSSCValidateCommand convertRowToCommand(Row salesRestrictionRow) {
		Cell cellReference = null;
		SalesRestrictionSSCValidateCommand commandReference = null;

		if (isRowHeaderSrPais(salesRestrictionRow)) {
			// Search header id
			cellReference = salesRestrictionRow.getCell(SalesRestrictionPosExcelColumnsEnum.SR_ID.getId());			
			salesRestrictionId = cellReference.getStringCellValue();
						
		}else {		
			// Search content
			if (!isRowHeaderOnlyAndIncludeAndExclude(salesRestrictionRow)) {				
				commandReference = new SalesRestrictionSSCValidateCommand();
				//
				commandReference.setSalesRestrictionId(salesRestrictionId);
				cellReference = salesRestrictionRow.getCell(SalesRestrictionPosExcelColumnsEnum.ONLY.getId());
				if (isCellValid(cellReference)) {
					commandReference.setOnly(cellReference.getStringCellValue());
				}

				cellReference = salesRestrictionRow.getCell(SalesRestrictionPosExcelColumnsEnum.INCLUDE.getId());
				if (isCellValid(cellReference)) {
					commandReference.setInclude(cellReference.getStringCellValue());
				}

				cellReference = salesRestrictionRow.getCell(SalesRestrictionPosExcelColumnsEnum.EXCLUDE.getId());
				if (isCellValid(cellReference)) {
					commandReference.setExclude(cellReference.getStringCellValue());
				}				
			} // end if
		}
		
		return commandReference;
	}
	
	
	protected boolean isRowHeaderSrPais(Row row){
		boolean isHeader = false;
		Cell cellSrPais = 
				row.getCell(SalesRestrictionPosExcelColumnsEnum.SR_PAIS.getId());
		if(cellSrPais.getStringCellValue().trim().equals(SalesRestrictionPosExcelColumnsEnum.SR_PAIS.getName())){
			isHeader = true;
		}
		return isHeader;
	}
	
	protected boolean isRowHeaderOnlyAndIncludeAndExclude(Row row){
		boolean isHeader = false;
		Cell cellOnly = 
				row.getCell(SalesRestrictionPosExcelColumnsEnum.ONLY.getId());		
		Cell cellInclude = 
				row.getCell(SalesRestrictionPosExcelColumnsEnum.INCLUDE.getId());
		Cell cellExclude = 
				row.getCell(SalesRestrictionPosExcelColumnsEnum.EXCLUDE.getId());
				
		if(cellOnly.getStringCellValue().trim().equals(SalesRestrictionPosExcelColumnsEnum.ONLY.getName()) &&
				cellInclude.getStringCellValue().trim().equals(SalesRestrictionPosExcelColumnsEnum.INCLUDE.getName()) &&
		   cellExclude.getStringCellValue().trim().equals(SalesRestrictionPosExcelColumnsEnum.EXCLUDE.getName())){
			isHeader = true;
		}		
		return isHeader;
	}	
		
	protected boolean isCommandNotEmpty(SalesRestrictionSSCValidateCommand command){		
		boolean  isCommandNotEmpty = false;
		if(command != null) {
			isCommandNotEmpty = true;
		}		
		return isCommandNotEmpty;
	}
	
	protected boolean isRowNotEmpty(Row row){		
		boolean isRowNotEmpty = false;
		for(Cell cell : row) {			
			if(isCellValid(cell) && isCellNotBlank(cell)) {
				isRowNotEmpty = true;
			}
		}
		
		return isRowNotEmpty;		
	}
	
	protected boolean isCellValid(Cell cell){
		boolean isValid = false;
		if(cell != null && cell.getCellType() == Cell.CELL_TYPE_STRING){
			isValid = true;
		}		
		return isValid;		
	}
	
	protected boolean isCellNotBlank(Cell cell){
		boolean isNotBlank = false;
		if(cell.getCellType() != Cell.CELL_TYPE_BLANK && StringUtils.isNotBlank(cell.getStringCellValue())){
			isNotBlank = true;			
		}
		return isNotBlank;		
	}

}
