package com.latam.rm.sparta.flows.function;

import java.util.function.Function;

import org.apache.poi.ss.usermodel.Row;

import com.latam.rm.sparta.flows.command.FareInterValidateCommand;
import com.latam.rm.sparta.flows.enums.ItemsInterExcelColumsEnum;

public class ItemsInterExcelToFareInterValidateCommandFunction implements Function<Row, FareInterValidateCommand> {

	@Override
	public FareInterValidateCommand apply(Row row) {
		FareInterValidateCommand command = new FareInterValidateCommand();

		command.setAction(
				row.getCell(ItemsInterExcelColumsEnum.ACTION.getId()).getStringCellValue());
		command.setCarrier(
				row.getCell(ItemsInterExcelColumsEnum.CARRIER.getId()).getStringCellValue());
		command.setOrigin(
				row.getCell(ItemsInterExcelColumsEnum.ORIGIN.getId()).getStringCellValue());
		command.setDestination(
				row.getCell(ItemsInterExcelColumsEnum.DESTINATION.getId()).getStringCellValue());
		command.setClassCode(
				row.getCell(ItemsInterExcelColumsEnum.CLASS.getId()).getStringCellValue());
		command.setSeason(
				row.getCell(ItemsInterExcelColumsEnum.SEASON.getId()).getStringCellValue());
		command.setDow(
				row.getCell(ItemsInterExcelColumsEnum.DOW.getId()).getStringCellValue());
		command.setDowMatch(
				row.getCell(ItemsInterExcelColumsEnum.DOW_MATCH.getId()).getStringCellValue());
		command.setFamily(
				row.getCell(ItemsInterExcelColumsEnum.FAMILY.getId()).getStringCellValue());
		//command.setApCode(
				//row.getCell(ItemsInterExcelColumsEnum.COD_AP.getId()).getStringCellValue());
		command.setApUser(
				row.getCell(ItemsInterExcelColumsEnum.USER_AP.getId()).getStringCellValue());
		//command.setMnCode(
				//row.getCell(ItemsInterExcelColumsEnum.COD_MN.getId()).getStringCellValue());
		command.setMnUser(
				row.getCell(ItemsInterExcelColumsEnum.USER_MN.getId()).getStringCellValue());
		
		//TODO Completar los demas campos
				
		return command;
	}

}
