package com.latam.rm.sparta.flows.repository;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.latam.rm.sparta.flows.dto.MinimumStayDto;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
@FeignClient("fare-integrity-query-side")
@RequestMapping("/minimunStay")
public interface MinimumStayRepository {

	@RequestMapping(method = RequestMethod.GET, path = "/getAll")
	public @ResponseBody Flux<MinimumStayDto> getAll();

	@RequestMapping(method = RequestMethod.GET, path = "/getAllByConfig")
	public @ResponseBody Flux<MinimumStayDto> getAllByConfig(@RequestParam String config);

	@RequestMapping(method = RequestMethod.GET, path = "/getByUserAndConfig")
	public @ResponseBody Mono<MinimumStayDto> getByUserAndConfig(
			@RequestParam String user, @RequestParam String config);	
						
}