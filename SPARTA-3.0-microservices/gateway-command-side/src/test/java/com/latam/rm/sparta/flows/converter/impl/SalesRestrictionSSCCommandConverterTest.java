package com.latam.rm.sparta.flows.converter.impl;

import static org.junit.Assert.assertNotNull;

import java.io.InputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.latam.rm.sparta.flows.command.SalesRestrictionSSCValidateCommand;
import com.latam.rm.sparta.flows.converter.CommandConverter;
import com.latam.rm.sparta.flows.converter.CommandConverterFactoryEnum;
import com.latam.rm.sparta.flows.dto.GatewayContextDto;
import com.monitorjbl.xlsx.StreamingReader;


public class SalesRestrictionSSCCommandConverterTest {
	
	Workbook workBook;
	
	@Before
	public void setUp() throws Exception {
	InputStream is = 
			SalesRestrictionSSCCommandConverterTest.class.getResourceAsStream("files/DOM-PE-20 filas.xlsx");
	workBook = 
			StreamingReader.builder().rowCacheSize(100).bufferSize(4096).open(is);		
	}

	@After	
	public void tearDown(){
	}
	
	@Test
	public void convertToCommand() throws Exception {
	Sheet salesRestrictionSheet = workBook.getSheet("SR");
	assertNotNull(workBook);
		
	CommandConverter converter =
					CommandConverterFactoryEnum.getConverter(CommandConverterFactoryEnum.SALES_RESTRICTION_DOMESTIC_SSC.getIdConverter());
	//
	List<SalesRestrictionSSCValidateCommand> list = (List<SalesRestrictionSSCValidateCommand>) converter.convertToCommand(salesRestrictionSheet, new GatewayContextDto());
	
	assertNotNull(list);
		
	}

}
