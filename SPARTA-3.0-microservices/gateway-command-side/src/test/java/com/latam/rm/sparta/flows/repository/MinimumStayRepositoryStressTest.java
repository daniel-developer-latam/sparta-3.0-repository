package com.latam.rm.sparta.flows.repository;

import static org.junit.Assert.assertNotNull;

import java.io.InputStream;

import org.apache.poi.ss.usermodel.Workbook;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.latam.rm.sparta.flows.dto.MinimumStayDto;
import com.monitorjbl.xlsx.StreamingReader;

import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MinimumStayRepositoryStressTest {

	@Autowired
	MinimumStayRepository repositoy;
	
	Workbook workBook;
	
	@Before
	public void setUp() throws Exception {
	InputStream is = 
			MinimumStayRepositoryStressTest.class.getResourceAsStream("files/1000000-Buenos-XLSX.xlsx");
	workBook = 
			StreamingReader.builder().rowCacheSize(100).bufferSize(4096).open(is);
		
	}

	@After	
	public void tearDown(){
	}
	
	@Test
	public void importerItems() throws Exception {
	Assert.assertNotNull(workBook);
	Assert.assertNotNull(repositoy);	
	
	Mono<MinimumStayDto> rxMinimumStayDto = repositoy.getByUserAndConfig("1 day", "DOM PE");
	
	assertNotNull(rxMinimumStayDto);	
	
	}	
	
}
