package com.latam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntegrityQuerySideApp {

	public static void main(String args[]) {
		SpringApplication.run(LegacyQuerySideApp.class, args);		
	}
}
