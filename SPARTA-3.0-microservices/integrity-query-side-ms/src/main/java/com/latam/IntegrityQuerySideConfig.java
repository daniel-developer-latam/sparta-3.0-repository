package com.latam;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableEurekaClient
@EnableAutoConfiguration
@MapperScan({"com.latam.pax.sparta.persistence.services"})
public class IntegrityQuerySideConfig {	
	
}
