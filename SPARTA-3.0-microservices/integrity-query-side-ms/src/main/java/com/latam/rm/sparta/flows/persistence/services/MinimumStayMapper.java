package com.latam.rm.sparta.flows.persistence.services;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.latam.rm.sparta.flows.dto.MinimumStayConfigDto;
import com.latam.rm.sparta.flows.persistence.exceptions.MinimumStayException;

@Mapper
public interface MinimumStayMapper {
	
	List<MinimumStayConfigDto> getAll() throws MinimumStayException;
	
}