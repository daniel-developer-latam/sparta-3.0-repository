package com.latam.rm.sparta.flows.persistence.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.latam.rm.sparta.flows.dto.MinimumStayConfigDto;
import com.latam.rm.sparta.flows.persistence.exceptions.MinimumStayException;

import lombok.extern.log4j.Log4j;

@Log4j
@Service
public class MinimumStayServices {

	@Autowired
	MinimumStayMapper mapper;

	List<MinimumStayConfigDto> getAll() {
		List<MinimumStayConfigDto> minimumStayConfigs = new ArrayList<>();						
		try {			
			minimumStayConfigs = mapper.getAll();
			
		} catch (MinimumStayException e) {
			log.error(e.getMessage(), e);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return minimumStayConfigs;
	}
}