package com.latam.pax.sparta.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value={ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ColumnAnnotation {
	String columnLetter();
	String message() default "";
	String source() default "";
	
	boolean change() default false;
	boolean conditionalExport() default false;
	String fieldToEvaluateForExport() default "";
	boolean expectedValue() default false;

	boolean footnote() default false;
	boolean translatenam() default false;
	boolean multiFootnote() default false;

	boolean format() default false;
	
}
