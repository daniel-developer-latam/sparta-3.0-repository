package com.latam.pax.sparta.persistence.criteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * 
 * Clase encargada de procesar las consultas que tienen mas de un parametro
 *
 */
public class Criteria implements Serializable{
	private static final String LIKE_COMPARATOR = "LIKE";
	private static final String EQUALS_COMPARATOR = "=";

	private static final long serialVersionUID = 1L;
	
	protected List<Predicate> predicates;

    public Criteria() {           
    	predicates = new ArrayList<Predicate>();
    }

    public void addSimplePredicate(String condition) {
        if (condition == null) {
            throw new IllegalArgumentException("Value for condition cannot be null");
        }
        predicates.add(new Predicate(condition));
    }
    
    public void addPredicate(Predicate value) {
        if (value == null) {
            throw new IllegalArgumentException("Predicate cannot be null");
        }
        predicates.add(value);
    }
    
    public void addPredicate(String condition, String comparator, String value) {
        if (value == null) {
            throw new IllegalArgumentException("Value cannot be null");
        }
        predicates.add(new Predicate(condition, comparator,  value));
    }
    
    public Predicate addInPredicate(String condition, List<String> value) {
        if (value == null || value.isEmpty()) {
            throw new IllegalArgumentException("List Value cannot be null or Empty");
        }
        Predicate p = new Predicate(condition, value);
		predicates.add(p);
		return p;
    }
       
    public Predicate addEqualsPredicate(String condition, String value) {
        if (value == null || condition == null ) {
            throw new IllegalArgumentException("Equal Value cannot be null");
        }
        Predicate p = new Predicate(condition, EQUALS_COMPARATOR,  value);
		predicates.add(p);
		return p;
    }
    
    public void addLikePredicate(String condition, String value) {
        if (value == null) {
            throw new IllegalArgumentException("Like Value cannot be null");
        }
        predicates.add(new Predicate("LOWER(" + condition + ")", LIKE_COMPARATOR,  value.toLowerCase()));
    }

    public void addBetweenPredicate(String condition, Object value1, Object value2) {
        if (value1 == null || value2 == null) {
            throw new IllegalArgumentException("Between values cannot be null");
        }
        predicates.add(new Predicate(condition, value1, value2));
    }
    

    public boolean isValid() {
        return !predicates.isEmpty();
    }

    public List<Predicate> getAllCriteria() {
        return predicates;
    }

    public List<Predicate> getCriteria() {
        return predicates;
    }
    
    public void addAllCriterion(List<Predicate> predicates){
    	 if (predicates != null) {
    		 predicates.addAll(predicates);
    	 }
    }

	@Override
	public String toString() {
		if(predicates != null){
			return predicates.toString();	
		}
		return "<Empty_Predicates>";
	}
    
    

   
}
