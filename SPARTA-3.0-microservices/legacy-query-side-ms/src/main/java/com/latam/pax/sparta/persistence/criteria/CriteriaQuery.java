package com.latam.pax.sparta.persistence.criteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Implementacion de Busquedas mediante criteria queries con paginado.
 * @author jonathan loyola
 *
 */
public class CriteriaQuery implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5689351573883858162L;
	private int pageSize;
	private int first;

	protected String orderByClause;
	protected boolean distinct;
	protected List<Criteria> andCriterias;
	//This is a list of counts that will represent the report data, for reports
	private String metrics;

	public CriteriaQuery() {
		andCriterias = new ArrayList<Criteria>();
	}
	
	

	public void and(Criteria criteria) {
		andCriterias.add(criteria);
	}

	public Criteria and() {
		Criteria criteria = createCriteriaInternal();
		andCriterias.add(criteria);
		return criteria;
	}

	protected Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (andCriterias.isEmpty()) {
			andCriterias.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		andCriterias.clear();
		orderByClause = null;
		distinct = false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(1024);// (128);
		sb.append(this.getClass().getSimpleName()).append('[');

		if (!StringUtils.isEmpty(orderByClause)) {
			sb.append("orderByClause=").append(orderByClause);
		}
		if (distinct) {
			sb.append(",distinct=").append(distinct);
		}
		if (andCriterias != null && !andCriterias.isEmpty()) {
			sb.append(",And_Criterias=").append(andCriterias);
		}
		sb.append(']');
		return sb.toString();
	}
	
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}	

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the first
	 */
	public int getFirst() {
		return first;
	}

	/**
	 * @param first the first to set
	 */
	public void setFirst(int first) {
		this.first = first;
	}

	public String getMetrics() {
		return metrics;
	}

	public void setMetrics(String metrics) {
		this.metrics = metrics;
	}

}
