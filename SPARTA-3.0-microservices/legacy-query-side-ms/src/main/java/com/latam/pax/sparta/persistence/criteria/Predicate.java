package com.latam.pax.sparta.persistence.criteria;

import java.io.Serializable;
import java.util.List;


import lombok.EqualsAndHashCode;

/**
 * @author jonathan loyola
 *
 * Clase que se encarga de procesar los parametros de las consultas
 */
@EqualsAndHashCode
public class Predicate implements Serializable {
	private static final long serialVersionUID = 1L;

	private String condition;	
	private String comparator; 
	private Object value;

    private Object secondValue;

    private boolean noValue;

    private boolean singleValue;

    private boolean betweenValue;

    private boolean listValue;

    protected Predicate(String condition) {
        super();
        this.condition = (condition);
        this.noValue = true;
    }
    
    protected Predicate(String condition, String comparator, Object value) {
        super();
        this.condition = (condition);
        this.comparator = (comparator);
        this.value = (value);
        if (value instanceof List<?>) {
            this.listValue = true;
        } else {
            this.singleValue = true;
        }
    }

    protected Predicate(String condition, Object value) {
        super();
        this.condition = (condition);
        this.value = (value);
        if (value instanceof List<?>) {
            this.listValue = true;
        } else {
            this.singleValue = true;
        }
    }

    protected Predicate(String condition, Object value, Object secondValue) {
        super();
        this.condition = condition;
        this.value = value;
        this.secondValue = (secondValue);
        this.betweenValue = true;
    }
//	/**
//	 * @param condition the condition to set
//	 */
//	public void setCondition(String condition) {
//		this.condition = condition;
//	}
//
//	/**
//	 * @param value the value to set
//	 */
//	public void setValue(Object value) {
//		this.value = value;
//	}
//
//	/**
//	 * @param secondValue the secondValue to set
//	 */
//	public void setSecondValue(Object secondValue) {
//		this.secondValue = secondValue;
//	}
	
    public String getCondition() {
        return condition;
    }
    
    public String getComparator() {
  		return comparator;
  	}

    public Object getValue() {
        return value;
    }

    public Object getSecondValue() {
        return secondValue;
    }

    public boolean isNoValue() {
        return noValue;
    }

    public boolean isSingleValue() {
        return singleValue;
    }

    public boolean isBetweenValue() {
        return betweenValue;
    }

    public boolean isListValue() {
        return listValue;
    }

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(1024);
		sb.append(this.getClass().getSimpleName()).append('[');

		if (noValue) {
			sb.append("condition=").append(condition);
		}
		if (singleValue) {
			sb.append(condition).append(' ').append(comparator).append(' ').append(value);
		}
		if (betweenValue) {
			sb.append(condition).append(" BETWEEN ").append(value)
					.append(secondValue);
		}
		if (listValue) {
			sb.append(condition).append(" IN ").append(value);
		}
		sb.append(']');
		return sb.toString();
		
	}
	
	
}