//display.label.ParametricAttType.paattyId=paattyId
//domain.ParametricAttType.paattyId= No puede ser null.
//domain.ParametricAttType.paattyId.lenght= No puede ser mayor a 40
//display.label.ParametricAttType.description=description
//domain.ParametricAttType.description.lenght= No puede ser mayor a 512
//display.label.ParametricAttType.wsFlg=wsFlg
//domain.ParametricAttType.wsFlg.lenght= No puede ser mayor a 1
//display.label.ParametricAttType.variablesNumber=variablesNumber
//display.label.ParametricAttType.fifthLabelValue=fifthLabelValue
//domain.ParametricAttType.fifthLabelValue.lenght= No puede ser mayor a 30
//display.label.ParametricAttType.fourthLabelValue=fourthLabelValue
//domain.ParametricAttType.fourthLabelValue.lenght= No puede ser mayor a 30
//display.label.ParametricAttType.thirdLabelValue=thirdLabelValue
//domain.ParametricAttType.thirdLabelValue.lenght= No puede ser mayor a 30
//display.label.ParametricAttType.secondLabelValue=secondLabelValue
//domain.ParametricAttType.secondLabelValue.lenght= No puede ser mayor a 30
//display.label.ParametricAttType.keyLabelValue=keyLabelValue
//domain.ParametricAttType.keyLabelValue.lenght= No puede ser mayor a 30
//display.label.ParametricAttType.activeFlg=activeFlg
//domain.ParametricAttType.activeFlg.lenght= No puede ser mayor a 1
//display.label.ParametricAttType.keyLengthNumber=keyLengthNumber
//domain.ParametricAttType.keyLengthNumber= No puede ser null.
//display.label.ParametricAttType.fifthLengthNumber=fifthLengthNumber
//domain.ParametricAttType.fifthLengthNumber= No puede ser null.
//display.label.ParametricAttType.fourthLengthNumber=fourthLengthNumber
//domain.ParametricAttType.fourthLengthNumber= No puede ser null.
//display.label.ParametricAttType.thirdLengthNumber=thirdLengthNumber
//domain.ParametricAttType.thirdLengthNumber= No puede ser null.
//display.label.ParametricAttType.secondLengthNumber=secondLengthNumber
//domain.ParametricAttType.secondLengthNumber= No puede ser null.
package com.latam.pax.sparta.persistence.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class ParametricAttType extends ParametricAttTypeParent implements Serializable {
    
    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_FOURTH_LABEL
     *

     */
    @Length(max=30 , message="{domain.ParametricAttType.fourthLabelValue.lenght}")
    private String fourthLabelValue;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_THIRD_LABEL
     *

     */
    @Length(max=30 , message="{domain.ParametricAttType.thirdLabelValue.lenght}")
    private String thirdLabelValue;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_SECOND_LABEL
     *
     */
    @Length(max=30 , message="{domain.ParametricAttType.secondLabelValue.lenght}")
    private String secondLabelValue;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_KEY_LABEL
     *

     */
    @Length(max=30 , message="{domain.ParametricAttType.keyLabelValue.lenght}")
    private String keyLabelValue;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_FLG_ACTIVE
     *

     */
    @Length(max=1 , message="{domain.ParametricAttType.activeFlg.lenght}")
    private String activeFlg;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_KEY_LENGTH
     *

     */
    @NotNull(message="{domain.ParametricAttType.keyLengthNumber}")
    private int keyLengthNumber;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_FIFTH_LENGTH
     *

     */
    @NotNull(message="{domain.ParametricAttType.fifthLengthNumber}")
    private int fifthLengthNumber;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_FOURTH_LENGTH
     *

     */
    @NotNull(message="{domain.ParametricAttType.fourthLengthNumber}")
    private int fourthLengthNumber;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_THIRD_LENGTH
     *

     */
    @NotNull(message="{domain.ParametricAttType.thirdLengthNumber}")
    private int thirdLengthNumber;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_SECOND_LENGTH
     *

     */
    @NotNull(message="{domain.ParametricAttType.secondLengthNumber}")
    private int secondLengthNumber;

    /**
     * This field corresponds to the database table EXSPARTA.PARAMETRIC_ATT_TYPES
     *

     */
    private static final long serialVersionUID = 1L;
    
    private List<ParametricAttValue> parametricAttValuesList;

    public List<ParametricAttValue> getParametricAttValuesList() {
		return parametricAttValuesList;
	}

	public void setParametricAttValuesList(
			List<ParametricAttValue> parametricAttValuesList) {
		this.parametricAttValuesList = parametricAttValuesList;
	}
	
	public void addParametricAttValue() {
		if(this.parametricAttValuesList == null){
			this.parametricAttValuesList = new ArrayList<ParametricAttValue>();
		}
		ParametricAttValue parametricAttItem = new ParametricAttValue();
		this.parametricAttValuesList.add(parametricAttItem);
		
	}
	
	public void removeParametricAttValue(ParametricAttValue parametricAtt) {
		if(this.parametricAttValuesList != null){
			this.parametricAttValuesList.remove(parametricAtt);
		}
	}


    

    

    /**
     * Largo:30
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_FOURTH_LABEL
     *

     */
    public String getFourthLabelValue() {
        return fourthLabelValue;
    }

    /**
     * Largo:30
     *
     * @param fourthLabelValue the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_FOURTH_LABEL
     *

     */
    public void setFourthLabelValue(String fourthLabelValue) {
        this.fourthLabelValue = fourthLabelValue == null ? null : fourthLabelValue.trim();
    }

    /**
     * Largo:30
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_THIRD_LABEL
     *

     */
    public String getThirdLabelValue() {
        return thirdLabelValue;
    }

    /**
     * Largo:30
     *
     * @param thirdLabelValue the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_THIRD_LABEL
     *

     */
    public void setThirdLabelValue(String thirdLabelValue) {
        this.thirdLabelValue = thirdLabelValue == null ? null : thirdLabelValue.trim();
    }

    /**
     * Largo:30
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_SECOND_LABEL
     *

     */
    public String getSecondLabelValue() {
        return secondLabelValue;
    }

    /**
     * Largo:30
     *
     * @param secondLabelValue the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_SECOND_LABEL
     *

     */
    public void setSecondLabelValue(String secondLabelValue) {
        this.secondLabelValue = secondLabelValue == null ? null : secondLabelValue.trim();
    }

    /**
     * Largo:30
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_KEY_LABEL
     *

     */
    public String getKeyLabelValue() {
        return keyLabelValue;
    }

    /**
     * Largo:30
     *
     * @param keyLabelValue the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_KEY_LABEL
     *

     */
    public void setKeyLabelValue(String keyLabelValue) {
        this.keyLabelValue = keyLabelValue == null ? null : keyLabelValue.trim();
    }

    /**
     * Largo:1
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_FLG_ACTIVE
     *

     */
    public String getActiveFlg() {
        return activeFlg;
    }

    /**
     * Largo:1
     *
     * @param activeFlg the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_FLG_ACTIVE
     *

     */
    public void setActiveFlg(String activeFlg) {
        this.activeFlg = activeFlg == null ? null : activeFlg.trim();
    }

    /**
     * Largo:22
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_KEY_LENGTH
     *

     */
    public int getKeyLengthNumber() {
        return keyLengthNumber;
    }

    /**
     * Largo:22
     *
     * @param keyLengthNumber the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_KEY_LENGTH
     *

     */
    public void setKeyLengthNumber(int keyLengthNumber) {
        this.keyLengthNumber = keyLengthNumber;
    }

    /**
     * Largo:22
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_FIFTH_LENGTH
     *

     */
    public int getFifthLengthNumber() {
        return fifthLengthNumber;
    }

    /**
     * Largo:22
     *
     * @param fifthLengthNumber the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_FIFTH_LENGTH
     *

     */
    public void setFifthLengthNumber(int fifthLengthNumber) {
        this.fifthLengthNumber = fifthLengthNumber;
    }

    /**
     * Largo:22
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_FOURTH_LENGTH
     *

     */
    public int getFourthLengthNumber() {
        return fourthLengthNumber;
    }

    /**
     * Largo:22
     *
     * @param fourthLengthNumber the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_FOURTH_LENGTH
     *

     */
    public void setFourthLengthNumber(int fourthLengthNumber) {
        this.fourthLengthNumber = fourthLengthNumber;
    }

    /**
     * Largo:22
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_THIRD_LENGTH
     *

     */
    public int getThirdLengthNumber() {
        return thirdLengthNumber;
    }

    /**
     * Largo:22
     *
     * @param thirdLengthNumber the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_THIRD_LENGTH
     *

     */
    public void setThirdLengthNumber(int thirdLengthNumber) {
        this.thirdLengthNumber = thirdLengthNumber;
    }

    /**
     * Largo:22
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_SECOND_LENGTH
     *

     */
    public int getSecondLengthNumber() {
        return secondLengthNumber;
    }

    /**
     * Largo:22
     *
     * @param secondLengthNumber the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_SECOND_LENGTH
     *

     */
    public void setSecondLengthNumber(int secondLengthNumber) {
        this.secondLengthNumber = secondLengthNumber;
    }

    

	/**
     * This method corresponds to the database table EXSPARTA.PARAMETRIC_ATT_TYPES
     *

     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ParametricAttType other = (ParametricAttType) that;
        return (this.getPaattyId() == null ? other.getPaattyId() == null : this.getPaattyId().equals(other.getPaattyId()));
    }

    /**
     * This method corresponds to the database table EXSPARTA.PARAMETRIC_ATT_TYPES
     *

     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPaattyId() == null) ? 0 : getPaattyId().hashCode());
        return result;
    }

    /**
     * This method corresponds to the database table EXSPARTA.PARAMETRIC_ATT_TYPES
     *

     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(1024);//(128);
        sb.append(getClass().getName())
        
        .append(Integer.toHexString(System.identityHashCode(this)))
        .append('[')
        .append(super.toString())
        .append(",paattyId=").append(getPaattyId())
        .append(",description=").append(getDescription())
        .append(",wsFlg=").append(getWsFlg())
        .append(",variablesNumber=").append(getVariablesNumber())
        .append(",fifthLabelValue=").append(getFifthLabelValue())
        .append(",fourthLabelValue=").append(fourthLabelValue)
        .append(",thirdLabelValue=").append(thirdLabelValue)
        .append(",secondLabelValue=").append(secondLabelValue)
        .append(",keyLabelValue=").append(keyLabelValue)
        .append(",activeFlg=").append(activeFlg)
        .append(",keyLengthNumber=").append(keyLengthNumber)
        .append(",fifthLengthNumber=").append(fifthLengthNumber)
        .append(",fourthLengthNumber=").append(fourthLengthNumber)
        .append(",thirdLengthNumber=").append(thirdLengthNumber)
        .append(",secondLengthNumber=").append(secondLengthNumber)
        .append(",serialVersionUID=").append(serialVersionUID)
        .append(']');
        return sb.toString();
    }
}