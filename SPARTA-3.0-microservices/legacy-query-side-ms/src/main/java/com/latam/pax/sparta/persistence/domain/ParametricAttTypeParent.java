package com.latam.pax.sparta.persistence.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public abstract class ParametricAttTypeParent implements Serializable {

	/**
	 * this field does not belong to the table, is just a temporal value
	 */
	private String teconfId;
	
	/**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_ID
     *

     */
    @NotNull(message="{domain.ParametricAttType.paattyId}")
    @Length(max=40 , message="{domain.ParametricAttType.paattyId.lenght}")
    private String paattyId;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_DSC
     *

     */
    @Length(max=512 , message="{domain.ParametricAttType.description.lenght}")
    private String description;
    
    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_FLG_WS
     *

     */
    @Length(max=1 , message="{domain.ParametricAttType.wsFlg.lenght}")
    private String wsFlg;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_VARIABLES
     *

     */
    private int variablesNumber;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_FIFTH_LABEL
     *

     */
    @Length(max=30 , message="{domain.ParametricAttType.fifthLabelValue.lenght}")
    private String fifthLabelValue;

    
    
    
	
	public String getTeconfId() {
		return teconfId;
	}

	public void setTeconfId(String teconfId) {
		this.teconfId = teconfId;
	}
	
	/**
     * Largo:40
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_ID
     *

     */
    public String getPaattyId() {
        return paattyId;
    }

    /**
     * Largo:40
     *
     * @param paattyId the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_ID
     *

     */
    public void setPaattyId(String paattyId) {
        this.paattyId = paattyId == null ? null : paattyId.trim();
    }

    /**
     * Largo:512
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_DSC
     *

     */
    public String getDescription() {
        return description;
    }

    /**
     * Largo:512
     *
     * @param description the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_DSC
     *

     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }
    
    /**
     * Largo:1
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_FLG_WS
     *

     */
    public String getWsFlg() {
        return wsFlg;
    }

    /**
     * Largo:1
     *
     * @param wsFlg the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_FLG_WS
     *

     */
    public void setWsFlg(String wsFlg) {
        this.wsFlg = wsFlg == null ? null : wsFlg.trim();
    }

    /**
     * Largo:22
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_VARIABLES
     *

     */
    public int getVariablesNumber() {
        return variablesNumber;
    }

    /**
     * Largo:22
     *
     * @param variablesNumber the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_NMR_VARIABLES
     *

     */
    public void setVariablesNumber(int variablesNumber) {
        this.variablesNumber = variablesNumber;
    }

    /**
     * Largo:30
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_FIFTH_LABEL
     *

     */
    public String getFifthLabelValue() {
        return fifthLabelValue;
    }

    /**
     * Largo:30
     *
     * @param fifthLabelValue the value for EXSPARTA.PARAMETRIC_ATT_TYPES.PAATTY_VLR_FIFTH_LABEL
     *

     */
    public void setFifthLabelValue(String fifthLabelValue) {
        this.fifthLabelValue = fifthLabelValue == null ? null : fifthLabelValue.trim();
    }

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
    
    
	
}
