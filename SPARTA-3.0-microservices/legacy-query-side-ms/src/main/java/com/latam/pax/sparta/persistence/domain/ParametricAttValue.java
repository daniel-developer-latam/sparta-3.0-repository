//display.label.ParametricAttValue.paattyId=paattyId
//domain.ParametricAttValue.paattyId= No puede ser null.
//domain.ParametricAttValue.paattyId.lenght= No puede ser mayor a 40
//display.label.ParametricAttValue.keyValue=keyValue
//domain.ParametricAttValue.keyValue= No puede ser null.
//domain.ParametricAttValue.keyValue.lenght= No puede ser mayor a 40
//display.label.ParametricAttValue.teconfId=teconfId
//domain.ParametricAttValue.teconfId= No puede ser null.
//domain.ParametricAttValue.teconfId.lenght= No puede ser mayor a 20
//display.label.ParametricAttValue.secondValue=secondValue
//domain.ParametricAttValue.secondValue.lenght= No puede ser mayor a 40
//display.label.ParametricAttValue.fifthValue=fifthValue
//domain.ParametricAttValue.fifthValue.lenght= No puede ser mayor a 40
//display.label.ParametricAttValue.fourthValue=fourthValue
//domain.ParametricAttValue.fourthValue.lenght= No puede ser mayor a 40
//display.label.ParametricAttValue.thirdValue=thirdValue
//domain.ParametricAttValue.thirdValue.lenght= No puede ser mayor a 40
package com.latam.pax.sparta.persistence.domain;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

public class ParametricAttValue implements Serializable {
    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_VALUES.PAATTY_ID
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ParametricAttValue.paattyId}")
    @Length(max=40 , message="{domain.ParametricAttValue.paattyId.lenght}")
    private String paattyId;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_KEY
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ParametricAttValue.keyValue}")
    @Length(max=40 , message="{domain.ParametricAttValue.keyValue.lenght}")
    private String keyValue;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_VALUES.TECONF_ID
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ParametricAttValue.teconfId}")
    @Length(max=20 , message="{domain.ParametricAttValue.teconfId.lenght}")
    private String teconfId;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_SECOND
     *
     * @mbggenerated
     */
    @Length(max=1000 , message="{domain.ParametricAttValue.secondValue.lenght}")
    private String secondValue;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_FIFTH
     *
     * @mbggenerated
     */
    @Length(max=40 , message="{domain.ParametricAttValue.fifthValue.lenght}")
    private String fifthValue;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_FOURTH
     *
     * @mbggenerated
     */
    @Length(max=40 , message="{domain.ParametricAttValue.fourthValue.lenght}")
    private String fourthValue;

    /**
     * This field corresponds to the database column EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_THIRD
     *
     * @mbggenerated
     */
    @Length(max=40 , message="{domain.ParametricAttValue.thirdValue.lenght}")
    private String thirdValue;

    /**
     * This field corresponds to the database table EXSPARTA.PARAMETRIC_ATT_VALUES
     *
     * @mbggenerated
     */
    private static final long serialVersionUID = 1L;

    /**
     * Largo:40
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_VALUES.PAATTY_ID
     *
     * @mbggenerated
     */
    public String getPaattyId() {
        return paattyId;
    }

    /**
     * Largo:40
     *
     * @param paattyId the value for EXSPARTA.PARAMETRIC_ATT_VALUES.PAATTY_ID
     *
     * @mbggenerated
     */
    public void setPaattyId(String paattyId) {
        this.paattyId = paattyId == null ? null : paattyId.trim();
    }

    /**
     * Largo:40
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_KEY
     *
     * @mbggenerated
     */
    public String getKeyValue() {
        return keyValue;
    }

    /**
     * Largo:40
     *
     * @param keyValue the value for EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_KEY
     *
     * @mbggenerated
     */
    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue == null ? null : keyValue.trim();
    }

    /**
     * Largo:20
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_VALUES.TECONF_ID
     *
     * @mbggenerated
     */
    public String getTeconfId() {
        return teconfId;
    }

    /**
     * Largo:20
     *
     * @param teconfId the value for EXSPARTA.PARAMETRIC_ATT_VALUES.TECONF_ID
     *
     * @mbggenerated
     */
    public void setTeconfId(String teconfId) {
        this.teconfId = teconfId == null ? null : teconfId.trim();
    }

    /**
     * Largo:40
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_SECOND
     *
     * @mbggenerated
     */
    public String getSecondValue() {
        return secondValue;
    }

    /**
     * Largo:40
     *
     * @param secondValue the value for EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_SECOND
     *
     * @mbggenerated
     */
    public void setSecondValue(String secondValue) {
        this.secondValue = secondValue == null ? null : secondValue.trim();
    }

    /**
     * Largo:40
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_FIFTH
     *
     * @mbggenerated
     */
    public String getFifthValue() {
        return fifthValue;
    }

    /**
     * Largo:40
     *
     * @param fifthValue the value for EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_FIFTH
     *
     * @mbggenerated
     */
    public void setFifthValue(String fifthValue) {
        this.fifthValue = fifthValue == null ? null : fifthValue.trim();
    }

    /**
     * Largo:40
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_FOURTH
     *
     * @mbggenerated
     */
    public String getFourthValue() {
        return fourthValue;
    }

    /**
     * Largo:40
     *
     * @param fourthValue the value for EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_FOURTH
     *
     * @mbggenerated
     */
    public void setFourthValue(String fourthValue) {
        this.fourthValue = fourthValue == null ? null : fourthValue.trim();
    }

    /**
     * Largo:40
     *
     * @return the value of EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_THIRD
     *
     * @mbggenerated
     */
    public String getThirdValue() {
        return thirdValue;
    }

    /**
     * Largo:40
     *
     * @param thirdValue the value for EXSPARTA.PARAMETRIC_ATT_VALUES.PAATVA_VLR_THIRD
     *
     * @mbggenerated
     */
    public void setThirdValue(String thirdValue) {
        this.thirdValue = thirdValue == null ? null : thirdValue.trim();
    }

    /**
     * This method corresponds to the database table EXSPARTA.PARAMETRIC_ATT_VALUES
     *
     * @mbggenerated
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ParametricAttValue other = (ParametricAttValue) that;
        return (this.getPaattyId() == null ? other.getPaattyId() == null : this.getPaattyId().equals(other.getPaattyId()))
            && (this.getKeyValue() == null ? other.getKeyValue() == null : this.getKeyValue().equals(other.getKeyValue()))
            && (this.getTeconfId() == null ? other.getTeconfId() == null : this.getTeconfId().equals(other.getTeconfId()));
    }

    /**
     * This method corresponds to the database table EXSPARTA.PARAMETRIC_ATT_VALUES
     *
     * @mbggenerated
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPaattyId() == null) ? 0 : getPaattyId().hashCode());
        result = prime * result + ((getKeyValue() == null) ? 0 : getKeyValue().hashCode());
        result = prime * result + ((getTeconfId() == null) ? 0 : getTeconfId().hashCode());
        return result;
    }

    /**
     * This method corresponds to the database table EXSPARTA.PARAMETRIC_ATT_VALUES
     *
     * @mbggenerated
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(1024);//(128);
        sb.append(getClass().getName())
        
        .append(Integer.toHexString(System.identityHashCode(this)))
        .append('[')
        .append(super.toString())
        .append(",paattyId=").append(paattyId)
        .append(",keyValue=").append(keyValue)
        .append(",teconfId=").append(teconfId)
        .append(",secondValue=").append(secondValue)
        .append(",fifthValue=").append(fifthValue)
        .append(",fourthValue=").append(fourthValue)
        .append(",thirdValue=").append(thirdValue)
        .append(",serialVersionUID=").append(serialVersionUID)
        .append(']');
        return sb.toString();
    }
}