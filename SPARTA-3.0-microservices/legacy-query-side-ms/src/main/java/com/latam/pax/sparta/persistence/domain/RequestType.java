//display.label.RequestType.rqtypeId=rqtypeId
//domain.RequestType.rqtypeId= No puede ser null.
//domain.RequestType.rqtypeId.lenght= No puede ser mayor a 30
//display.label.RequestType.name=name
//domain.RequestType.name= No puede ser null.
//domain.RequestType.name.lenght= No puede ser mayor a 30
//display.label.RequestType.needAprobGerentFlg=needAprobGerentFlg
//domain.RequestType.needAprobGerentFlg.lenght= No puede ser mayor a 1
//display.label.RequestType.needAprobRmFlg=needAprobRmFlg
//domain.RequestType.needAprobRmFlg.lenght= No puede ser mayor a 1
//display.label.RequestType.needOdposFlg=needOdposFlg
//domain.RequestType.needOdposFlg.lenght= No puede ser mayor a 1
//display.label.RequestType.visibilityType=visibilityType
//domain.RequestType.visibilityType.lenght= No puede ser mayor a 20
//display.label.RequestType.processType=processType
//domain.RequestType.processType.lenght= No puede ser mayor a 20
//display.label.RequestType.teconfId=teconfId
//domain.RequestType.teconfId.lenght= No puede ser mayor a 20
//display.label.RequestType.rqtypeIdParent=rqtypeIdParent
//domain.RequestType.rqtypeIdParent.lenght= No puede ser mayor a 20
//display.label.RequestType.hierarchyFlg=hierarchyFlg
//domain.RequestType.hierarchyFlg.lenght= No puede ser mayor a 1
//display.label.RequestType.hasAttachmentsFlg=hasAttachmentsFlg
//domain.RequestType.hasAttachmentsFlg.lenght= No puede ser mayor a 1
package com.latam.pax.sparta.persistence.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class RequestType implements Serializable {
	
	private boolean aprobGte;
	private boolean aprobRM;
    /**
     * This field corresponds to the database column EXSPARTA.REQUEST_TYPES.RQTYPE_ID
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.RequestType.rqtypeId}")
    @Length(max=30 , message="{domain.RequestType.rqtypeId.lenght}")
    private String rqtypeId;

    /**
     * This field corresponds to the database column EXSPARTA.REQUEST_TYPES.RQTYPE_NMB
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.RequestType.name}")
    @Length(max=30 , message="{domain.RequestType.name.lenght}")
    private String name;

    /**
     * This field corresponds to the database column EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_NEED_APROB_GERENT
     *
     * @mbggenerated
     */
    @Length(max=1 , message="{domain.RequestType.needAprobGerentFlg.lenght}")
    private String needAprobGerentFlg;

    /**
     * This field corresponds to the database column EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_NEED_APROB_RM
     *
     * @mbggenerated
     */
    @Length(max=1 , message="{domain.RequestType.needAprobRmFlg.lenght}")
    private String needAprobRmFlg;

    /**
     * This field corresponds to the database column EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_NEED_ODPOS
     *
     * @mbggenerated
     */
    @Length(max=1 , message="{domain.RequestType.needOdposFlg.lenght}")
    private String needOdposFlg;

    /**
     * This field corresponds to the database column EXSPARTA.REQUEST_TYPES.RQTYPE_TPO_VISIBILITY
     *
     * @mbggenerated
     */
    @Length(max=20 , message="{domain.RequestType.visibilityType.lenght}")
    private String visibilityType;

    /**
     * This field corresponds to the database column EXSPARTA.REQUEST_TYPES.RQTYPE_TPO_PROCESS
     *
     * @mbggenerated
     */
    @Length(max=20 , message="{domain.RequestType.processType.lenght}")
    private String processType;

    /**
     * This field corresponds to the database column EXSPARTA.REQUEST_TYPES.TECONF_ID
     *
     * @mbggenerated
     */
    @Length(max=20 , message="{domain.RequestType.teconfId.lenght}")
    private String teconfId;

    /**
     * This field corresponds to the database column EXSPARTA.REQUEST_TYPES.RQTYPE_ID_PARENT
     *
     * @mbggenerated
     */
    @Length(max=20 , message="{domain.RequestType.rqtypeIdParent.lenght}")
    private String rqtypeIdParent;

    /**
     * This field corresponds to the database column EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_HIERARCHY
     *
     * @mbggenerated
     */
    @Length(max=1 , message="{domain.RequestType.hierarchyFlg.lenght}")
    private String hierarchyFlg;

    /**
     * This field corresponds to the database column EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_HAS_ATTACHMENTS
     *
     * @mbggenerated
     */
    @Length(max=1 , message="{domain.RequestType.hasAttachmentsFlg.lenght}")
    private String hasAttachmentsFlg;
    
    private Integer order;

    /**
     * This field corresponds to the database table EXSPARTA.REQUEST_TYPES
     *
     * @mbggenerated
     */
    private static final long serialVersionUID = 1L;

    /**
     * Largo:30
     *
     * @return the value of EXSPARTA.REQUEST_TYPES.RQTYPE_ID
     *
     * @mbggenerated
     */
    public String getRqtypeId() {
        return rqtypeId;
    }

    /**
     * Largo:30
     *
     * @param rqtypeId the value for EXSPARTA.REQUEST_TYPES.RQTYPE_ID
     *
     * @mbggenerated
     */
    public void setRqtypeId(String rqtypeId) {
        this.rqtypeId = rqtypeId == null ? null : rqtypeId.trim();
    }

    /**
     * Largo:30
     *
     * @return the value of EXSPARTA.REQUEST_TYPES.RQTYPE_NMB
     *
     * @mbggenerated
     */
    public String getName() {
        return name;
    }

    /**
     * Largo:30
     *
     * @param name the value for EXSPARTA.REQUEST_TYPES.RQTYPE_NMB
     *
     * @mbggenerated
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * Largo:1
     *
     * @return the value of EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_NEED_APROB_GERENT
     *
     * @mbggenerated
     */
    public String getNeedAprobGerentFlg() {
        return needAprobGerentFlg;
    }

    /**
     * Largo:1
     *
     * @param needAprobGerentFlg the value for EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_NEED_APROB_GERENT
     *
     * @mbggenerated
     */
    public void setNeedAprobGerentFlg(String needAprobGerentFlg) {
        this.needAprobGerentFlg = needAprobGerentFlg == null ? null : needAprobGerentFlg.trim();
    }

    /**
     * Largo:1
     *
     * @return the value of EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_NEED_APROB_RM
     *
     * @mbggenerated
     */
    public String getNeedAprobRmFlg() {
        return needAprobRmFlg;
    }

    /**
     * Largo:1
     *
     * @param needAprobRmFlg the value for EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_NEED_APROB_RM
     *
     * @mbggenerated
     */
    public void setNeedAprobRmFlg(String needAprobRmFlg) {
        this.needAprobRmFlg = needAprobRmFlg == null ? null : needAprobRmFlg.trim();
    }

    /**
     * Largo:1
     *
     * @return the value of EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_NEED_ODPOS
     *
     * @mbggenerated
     */
    public String getNeedOdposFlg() {
        return needOdposFlg;
    }

    /**
     * Largo:1
     *
     * @param needOdposFlg the value for EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_NEED_ODPOS
     *
     * @mbggenerated
     */
    public void setNeedOdposFlg(String needOdposFlg) {
        this.needOdposFlg = needOdposFlg == null ? null : needOdposFlg.trim();
    }

    /**
     * Largo:20
     *
     * @return the value of EXSPARTA.REQUEST_TYPES.RQTYPE_TPO_VISIBILITY
     *
     * @mbggenerated
     */
    public String getVisibilityType() {
        return visibilityType;
    }

    /**
     * Largo:20
     *
     * @param visibilityType the value for EXSPARTA.REQUEST_TYPES.RQTYPE_TPO_VISIBILITY
     *
     * @mbggenerated
     */
    public void setVisibilityType(String visibilityType) {
        this.visibilityType = visibilityType == null ? null : visibilityType.trim();
    }

    /**
     * Largo:20
     *
     * @return the value of EXSPARTA.REQUEST_TYPES.RQTYPE_TPO_PROCESS
     *
     * @mbggenerated
     */
    public String getProcessType() {
        return processType;
    }

    /**
     * Largo:20
     *
     * @param processType the value for EXSPARTA.REQUEST_TYPES.RQTYPE_TPO_PROCESS
     *
     * @mbggenerated
     */
    public void setProcessType(String processType) {
        this.processType = processType == null ? null : processType.trim();
    }

    /**
     * Largo:20
     *
     * @return the value of EXSPARTA.REQUEST_TYPES.TECONF_ID
     *
     * @mbggenerated
     */
    public String getTeconfId() {
        return teconfId;
    }

    /**
     * Largo:20
     *
     * @param teconfId the value for EXSPARTA.REQUEST_TYPES.TECONF_ID
     *
     * @mbggenerated
     */
    public void setTeconfId(String teconfId) {
        this.teconfId = teconfId == null ? null : teconfId.trim();
    }

    /**
     * Largo:20
     *
     * @return the value of EXSPARTA.REQUEST_TYPES.RQTYPE_ID_PARENT
     *
     * @mbggenerated
     */
    public String getRqtypeIdParent() {
        return rqtypeIdParent;
    }

    /**
     * Largo:20
     *
     * @param rqtypeIdParent the value for EXSPARTA.REQUEST_TYPES.RQTYPE_ID_PARENT
     *
     * @mbggenerated
     */
    public void setRqtypeIdParent(String rqtypeIdParent) {
        this.rqtypeIdParent = rqtypeIdParent == null ? null : rqtypeIdParent.trim();
    }

    /**
     * Largo:1
     *
     * @return the value of EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_HIERARCHY
     *
     * @mbggenerated
     */
    public String getHierarchyFlg() {
        return hierarchyFlg;
    }

    /**
     * Largo:1
     *
     * @param hierarchyFlg the value for EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_HIERARCHY
     *
     * @mbggenerated
     */
    public void setHierarchyFlg(String hierarchyFlg) {
        this.hierarchyFlg = hierarchyFlg == null ? null : hierarchyFlg.trim();
    }

    /**
     * Largo:1
     *
     * @return the value of EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_HAS_ATTACHMENTS
     *
     * @mbggenerated
     */
    public String getHasAttachmentsFlg() {
        return hasAttachmentsFlg;
    }

    /**
     * Largo:1
     *
     * @param hasAttachmentsFlg the value for EXSPARTA.REQUEST_TYPES.RQTYPE_FLG_HAS_ATTACHMENTS
     *
     * @mbggenerated
     */
    public void setHasAttachmentsFlg(String hasAttachmentsFlg) {
        this.hasAttachmentsFlg = hasAttachmentsFlg == null ? null : hasAttachmentsFlg.trim();
    }
    
    
    
    public boolean getAprobGte(){
    	
    	if(needAprobGerentFlg != null && !needAprobGerentFlg.trim().equals("")){
    		if(needAprobGerentFlg.trim().equals("Y")){
    			aprobGte = true;
    		}
    		else{
    			aprobGte = false;
    		}
    	}
    	else{
    		aprobGte = false;
    	}
    	
    	return aprobGte;
    }
    
    public void setAprobGte(boolean val){
    	if(val){
    		needAprobGerentFlg = "Y";
    	}
    	else{
    		needAprobGerentFlg = "N";
    	}
    	
    }
    
    
    
    public boolean getAprobRM(){
    	if(needAprobRmFlg != null && !needAprobRmFlg.trim().equals("")){
    		if(needAprobRmFlg.trim().equals("Y")){
    			aprobRM = true;
    		}
    		else{
    			aprobRM = false;
    		}
    	}
    	else{
    		aprobRM = false;
    	}
    	
    	return aprobRM;
    }
    
    public void setAprobRM(boolean val){
    	if(val){
    		needAprobRmFlg = "Y";
    	}
    	else{
    		needAprobRmFlg = "N";
    	}
    }

    public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	/**
     * This method corresponds to the database table EXSPARTA.REQUEST_TYPES
     *
     * @mbggenerated
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        RequestType other = (RequestType) that;
        return (this.getRqtypeId() == null ? other.getRqtypeId() == null : this.getRqtypeId().equals(other.getRqtypeId()));
    }

    /**
     * This method corresponds to the database table EXSPARTA.REQUEST_TYPES
     *
     * @mbggenerated
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getRqtypeId() == null) ? 0 : getRqtypeId().hashCode());
        return result;
    }

    /**
     * This method corresponds to the database table EXSPARTA.REQUEST_TYPES
     *
     * @mbggenerated
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(1024);//(128);
        sb.append(getClass().getName())
        
        .append(Integer.toHexString(System.identityHashCode(this)))
        .append('[')
        .append(super.toString())
        .append(",rqtypeId=").append(rqtypeId)
        .append(",name=").append(name)
        .append(",needAprobGerentFlg=").append(needAprobGerentFlg)
        .append(",needAprobRmFlg=").append(needAprobRmFlg)
        .append(",needOdposFlg=").append(needOdposFlg)
        .append(",visibilityType=").append(visibilityType)
        .append(",processType=").append(processType)
        .append(",teconfId=").append(teconfId)
        .append(",rqtypeIdParent=").append(rqtypeIdParent)
        .append(",hierarchyFlg=").append(hierarchyFlg)
        .append(",hasAttachmentsFlg=").append(hasAttachmentsFlg)
        .append(",serialVersionUID=").append(serialVersionUID)
        .append(']');
        return sb.toString();
    }
}