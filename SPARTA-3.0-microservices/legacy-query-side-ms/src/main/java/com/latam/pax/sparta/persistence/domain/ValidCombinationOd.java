//display.label.ValidCombinationOd.originValue=originValue
//domain.ValidCombinationOd.originValue= No puede ser null.
//domain.ValidCombinationOd.originValue.lenght= No puede ser mayor a 20
//display.label.ValidCombinationOd.destValue=destValue
//domain.ValidCombinationOd.destValue= No puede ser null.
//domain.ValidCombinationOd.destValue.lenght= No puede ser mayor a 20
//display.label.ValidCombinationOd.cxrParam=cxrParam
//domain.ValidCombinationOd.cxrParam= No puede ser null.
//domain.ValidCombinationOd.cxrParam.lenght= No puede ser mayor a 512
//display.label.ValidCombinationOd.cabinParam=cabinParam
//domain.ValidCombinationOd.cabinParam= No puede ser null.
//domain.ValidCombinationOd.cabinParam.lenght= No puede ser mayor a 512
//display.label.ValidCombinationOd.marketValue=marketValue
//domain.ValidCombinationOd.marketValue= No puede ser null.
//domain.ValidCombinationOd.marketValue.lenght= No puede ser mayor a 2
//display.label.ValidCombinationOd.routingNumber=routingNumber
//domain.ValidCombinationOd.routingNumber= No puede ser null.
//display.label.ValidCombinationOd.tarifPrivate2Number=tarifPrivate2Number
//domain.ValidCombinationOd.tarifPrivate2Number= No puede ser null.
//display.label.ValidCombinationOd.tarifPrivate1Number=tarifPrivate1Number
//domain.ValidCombinationOd.tarifPrivate1Number= No puede ser null.
//display.label.ValidCombinationOd.tarifViaPaNumber=tarifViaPaNumber
//domain.ValidCombinationOd.tarifViaPaNumber= No puede ser null.
//display.label.ValidCombinationOd.tarifViaAtNumber=tarifViaAtNumber
//domain.ValidCombinationOd.tarifViaAtNumber= No puede ser null.
//display.label.ValidCombinationOd.surchargeValue=surchargeValue
//domain.ValidCombinationOd.surchargeValue.lenght= No puede ser mayor a 10
//display.label.ValidCombinationOd.maxAmount2Number=maxAmount2Number
//domain.ValidCombinationOd.maxAmount2Number= No puede ser null.
//display.label.ValidCombinationOd.maxAmount1Number=maxAmount1Number
//domain.ValidCombinationOd.maxAmount1Number= No puede ser null.
//display.label.ValidCombinationOd.minAmount2Number=minAmount2Number
//domain.ValidCombinationOd.minAmount2Number= No puede ser null.
//display.label.ValidCombinationOd.minAmount1Number=minAmount1Number
//domain.ValidCombinationOd.minAmount1Number= No puede ser null.
//display.label.ValidCombinationOd.currency2Param=currency2Param
//domain.ValidCombinationOd.currency2Param= No puede ser null.
//domain.ValidCombinationOd.currency2Param.lenght= No puede ser mayor a 3
//display.label.ValidCombinationOd.currency1Param=currency1Param
//domain.ValidCombinationOd.currency1Param= No puede ser null.
//domain.ValidCombinationOd.currency1Param.lenght= No puede ser mayor a 3
//display.label.ValidCombinationOd.kmsFlg=kmsFlg
//domain.ValidCombinationOd.kmsFlg.lenght= No puede ser mayor a 1
//display.label.ValidCombinationOd.modifDate=modifDate
//domain.ValidCombinationOd.modifDate= No puede ser null.
//display.label.ValidCombinationOd.createdDate=createdDate
//domain.ValidCombinationOd.createdDate= No puede ser null.
//display.label.ValidCombinationOd.destRegionValue=destRegionValue
//domain.ValidCombinationOd.destRegionValue= No puede ser null.
//domain.ValidCombinationOd.destRegionValue.lenght= No puede ser mayor a 20
//display.label.ValidCombinationOd.destCountryValue=destCountryValue
//domain.ValidCombinationOd.destCountryValue= No puede ser null.
//domain.ValidCombinationOd.destCountryValue.lenght= No puede ser mayor a 20
//display.label.ValidCombinationOd.originRegionValue=originRegionValue
//domain.ValidCombinationOd.originRegionValue= No puede ser null.
//domain.ValidCombinationOd.originRegionValue.lenght= No puede ser mayor a 20
//display.label.ValidCombinationOd.originCountryValue=originCountryValue
//domain.ValidCombinationOd.originCountryValue= No puede ser null.
//domain.ValidCombinationOd.originCountryValue.lenght= No puede ser mayor a 20
//display.label.ValidCombinationOd.posValue=posValue
//domain.ValidCombinationOd.posValue.lenght= No puede ser mayor a 1
//display.label.ValidCombinationOd.activeFlg=activeFlg
//domain.ValidCombinationOd.activeFlg.lenght= No puede ser mayor a 1
package com.latam.pax.sparta.persistence.domain;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.latam.pax.sparta.annotation.ColumnAnnotation;

public class ValidCombinationOd implements Serializable {
	private boolean valid = true;
	private String warningMessage;
	
	public static final String KMS = "KMS";
	private static final String YES = "Y";
	private static final String NO = "N";	
    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_ORIGIN
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.originValue}")
    @Length(max=20 , message="{domain.ValidCombinationOd.originValue.lenght}")
    @ColumnAnnotation(columnLetter="B", message="{}")
    private String originValue;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_DEST
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.destValue}")
    @Length(max=20 , message="{domain.ValidCombinationOd.destValue.lenght}")
    @ColumnAnnotation(columnLetter="C", message="{}")
    private String destValue;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_PRM_CXR
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.cxrParam}")
    @Length(max=512 , message="{domain.ValidCombinationOd.cxrParam.lenght}")
    @ColumnAnnotation(columnLetter="A", message="{}")
    private String cxrParam;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_PRM_CABIN
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.cabinParam}")
    @Length(max=512 , message="{domain.ValidCombinationOd.cabinParam.lenght}")
    @ColumnAnnotation(columnLetter="D", message="{}")
    private String cabinParam;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_MARKET
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.marketValue}")
    @Length(max=2 , message="{domain.ValidCombinationOd.marketValue.lenght}")
    @ColumnAnnotation(columnLetter="V", message="{}")
    private String marketValue;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_ROUTING
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.routingNumber}")
    @ColumnAnnotation(columnLetter="U", message="{}")
    private Short routingNumber;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_TARIF_PRIVATE2
     *
     * @mbggenerated
     */
    @ColumnAnnotation(columnLetter="T", message="{}")
    private Short tarifPrivate2Number;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_TARIF_PRIVATE1
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.tarifPrivate1Number}")
    @ColumnAnnotation(columnLetter="S", message="{}")
    private Short tarifPrivate1Number;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_TARIF_VIA_PA
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.tarifViaPaNumber}")
    @ColumnAnnotation(columnLetter="R", message="{}")
    private Short tarifViaPaNumber;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_TARIF_VIA_AT
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.tarifViaAtNumber}")
    @ColumnAnnotation(columnLetter="Q", message="{}")
    private Short tarifViaAtNumber;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_SURCHARGE
     *
     * @mbggenerated
     */
    @Length(max=10 , message="{domain.ValidCombinationOd.surchargeValue.lenght}")
    @ColumnAnnotation(columnLetter="O", message="{}")
    private String surchargeValue;

    
    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_MAX_AMOUNT2
     *
     * @mbggenerated
     */
    @ColumnAnnotation(columnLetter="N", message="{}")
    private Integer maxAmount2Number;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_MAX_AMOUNT1
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.maxAmount1Number}")
    @ColumnAnnotation(columnLetter="M", message="{}")
    private Integer maxAmount1Number;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_MIN_AMOUNT2
     *
     * @mbggenerated
     */
    @ColumnAnnotation(columnLetter="L", message="{}")
    private Integer minAmount2Number;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_MIN_AMOUNT1
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.minAmount1Number}")
    @ColumnAnnotation(columnLetter="K", message="{}")
    private Integer minAmount1Number;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_PRM_CURRENCY2
     *
     * @mbggenerated
     */
    @Length(max=3 , message="{domain.ValidCombinationOd.currency2Param.lenght}")
    @ColumnAnnotation(columnLetter="J", message="{}")
    private String currency2Param;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_PRM_CURRENCY1
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.currency1Param}")
    @Length(max=3 , message="{domain.ValidCombinationOd.currency1Param.lenght}")
    @ColumnAnnotation(columnLetter="I", message="{}")
    private String currency1Param;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_FLG_KMS
     *
     * @mbggenerated
     */
    @Length(max=1 , message="{domain.ValidCombinationOd.kmsFlg.lenght}")
    private String kmsFlg;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_FCH_MODIF
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.modifDate}")
    private Date modifDate;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_FCH_CREATED
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.createdDate}")
    private Date createdDate;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_DEST_REGION
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.destRegionValue}")
    @Length(max=20 , message="{domain.ValidCombinationOd.destRegionValue.lenght}")
    @ColumnAnnotation(columnLetter="H", message="{}")
    private String destRegionValue;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_DEST_COUNTRY
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.destCountryValue}")
    @Length(max=20 , message="{domain.ValidCombinationOd.destCountryValue.lenght}")
    @ColumnAnnotation(columnLetter="F", message="{}")
    private String destCountryValue;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_ORIGIN_REGION
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.originRegionValue}")
    @Length(max=20 , message="{domain.ValidCombinationOd.originRegionValue.lenght}")
    @ColumnAnnotation(columnLetter="G", message="{}")
    private String originRegionValue;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_ORIGIN_COUNTRY
     *
     * @mbggenerated
     */
    @NotNull(message="{domain.ValidCombinationOd.originCountryValue}")
    @Length(max=20 , message="{domain.ValidCombinationOd.originCountryValue.lenght}")
    @ColumnAnnotation(columnLetter="E", message="{}")
    private String originCountryValue;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_POS
     *
     * @mbggenerated
     */
    @Length(max=1 , message="{domain.ValidCombinationOd.posValue.lenght}")
    @ColumnAnnotation(columnLetter="P", message="{}")
    private String posValue;

    /**
     * This field corresponds to the database column EXSPARTA.VALID_COMBINATION_OD.VCOMOD_FLG_ACTIVE
     *
     * @mbggenerated
     */
    @Length(max=1 , message="{domain.ValidCombinationOd.activeFlg.lenght}")
    @Pattern(regexp="Y|N" , message="{}")
    @ColumnAnnotation(columnLetter="W", message="{}")
    private String activeFlg = "Y";

    /**
     * This field corresponds to the database table EXSPARTA.VALID_COMBINATION_OD
     *
     * @mbggenerated
     */
    private static final long serialVersionUID = 1L;
    
    public Boolean getActive() {
		return YES.equals(this.activeFlg);
	}

	public void setActive(Boolean active) {
		this.activeFlg = active ? YES : NO;
	}    

    /**
     * Largo:20
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_ORIGIN
     *
     * @mbggenerated
     */
    public String getOriginValue() {
        return originValue;
    }

    /**
     * Largo:20
     *
     * @param originValue the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_ORIGIN
     *
     * @mbggenerated
     */
    public void setOriginValue(String originValue) {
        this.originValue = originValue == null ? null : originValue.trim();
    }

    /**
     * Largo:20
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_DEST
     *
     * @mbggenerated
     */
    public String getDestValue() {
        return destValue;
    }

    /**
     * Largo:20
     *
     * @param destValue the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_DEST
     *
     * @mbggenerated
     */
    public void setDestValue(String destValue) {
        this.destValue = destValue == null ? null : destValue.trim();
    }

    /**
     * Largo:512
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_PRM_CXR
     *
     * @mbggenerated
     */
    public String getCxrParam() {
        return cxrParam;
    }

    /**
     * Largo:512
     *
     * @param cxrParam the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_PRM_CXR
     *
     * @mbggenerated
     */
    public void setCxrParam(String cxrParam) {
        this.cxrParam = cxrParam == null ? null : cxrParam.trim();
    }

    /**
     * Largo:512
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_PRM_CABIN
     *
     * @mbggenerated
     */
    public String getCabinParam() {
        return cabinParam;
    }

    /**
     * Largo:512
     *
     * @param cabinParam the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_PRM_CABIN
     *
     * @mbggenerated
     */
    public void setCabinParam(String cabinParam) {
        this.cabinParam = cabinParam == null ? null : cabinParam.trim();
    }

    /**
     * Largo:2
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_MARKET
     *
     * @mbggenerated
     */
    public String getMarketValue() {
        return marketValue;
    }

    /**
     * Largo:2
     *
     * @param marketValue the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_MARKET
     *
     * @mbggenerated
     */
    public void setMarketValue(String marketValue) {
        this.marketValue = marketValue == null ? null : marketValue.trim();
    }

    /**
     * Largo:4
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_ROUTING
     *
     * @mbggenerated
     */
    public Short getRoutingNumber() {
        return routingNumber;
    }

    /**
     * Largo:4
     *
     * @param routingNumber the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_ROUTING
     *
     * @mbggenerated
     */
    public void setRoutingNumber(Short routingNumber) {
        this.routingNumber = routingNumber;
    }

    /**
     * Largo:3
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_TARIF_PRIVATE2
     *
     * @mbggenerated
     */
    public Short getTarifPrivate2Number() {
        return tarifPrivate2Number;
    }

    /**
     * Largo:3
     *
     * @param tarifPrivate2Number the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_TARIF_PRIVATE2
     *
     * @mbggenerated
     */
    public void setTarifPrivate2Number(Short tarifPrivate2Number) {
        this.tarifPrivate2Number = tarifPrivate2Number;
    }

    /**
     * Largo:3
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_TARIF_PRIVATE1
     *
     * @mbggenerated
     */
    public Short getTarifPrivate1Number() {
        return tarifPrivate1Number;
    }

    /**
     * Largo:3
     *
     * @param tarifPrivate1Number the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_TARIF_PRIVATE1
     *
     * @mbggenerated
     */
    public void setTarifPrivate1Number(Short tarifPrivate1Number) {
        this.tarifPrivate1Number = tarifPrivate1Number;
    }

    /**
     * Largo:3
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_TARIF_VIA_PA
     *
     * @mbggenerated
     */
    public Short getTarifViaPaNumber() {
        return tarifViaPaNumber;
    }

    /**
     * Largo:3
     *
     * @param tarifViaPaNumber the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_TARIF_VIA_PA
     *
     * @mbggenerated
     */
    public void setTarifViaPaNumber(Short tarifViaPaNumber) {
        this.tarifViaPaNumber = tarifViaPaNumber;
    }

    /**
     * Largo:3
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_TARIF_VIA_AT
     *
     * @mbggenerated
     */
    public Short getTarifViaAtNumber() {
        return tarifViaAtNumber;
    }

    /**
     * Largo:3
     *
     * @param tarifViaAtNumber the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_TARIF_VIA_AT
     *
     * @mbggenerated
     */
    public void setTarifViaAtNumber(Short tarifViaAtNumber) {
        this.tarifViaAtNumber = tarifViaAtNumber;
    }

    /**
     * Largo:10
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_SURCHARGE
     *
     * @mbggenerated
     */
    public String getSurchargeValue() {
        return surchargeValue;
    }

    /**
     * Largo:10
     *
     * @param surchargeValue the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_SURCHARGE
     *
     * @mbggenerated
     */
    public void setSurchargeValue(String surchargeValue) {
        this.surchargeValue = surchargeValue == null ? null : surchargeValue.trim();
    }
    
    /**
     * Largo:8
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_MAX_AMOUNT2
     *
     * @mbggenerated
     */
    public Integer getMaxAmount2Number() {
        return maxAmount2Number;
    }

    /**
     * Largo:8
     *
     * @param maxAmount2Number the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_MAX_AMOUNT2
     *
     * @mbggenerated
     */
    public void setMaxAmount2Number(Integer maxAmount2Number) {
        this.maxAmount2Number = maxAmount2Number;
    }

    /**
     * Largo:8
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_MAX_AMOUNT1
     *
     * @mbggenerated
     */
    public Integer getMaxAmount1Number() {
        return maxAmount1Number;
    }

    /**
     * Largo:8
     *
     * @param maxAmount1Number the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_MAX_AMOUNT1
     *
     * @mbggenerated
     */
    public void setMaxAmount1Number(Integer maxAmount1Number) {
        this.maxAmount1Number = maxAmount1Number;
    }

    /**
     * Largo:8
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_MIN_AMOUNT2
     *
     * @mbggenerated
     */
    public Integer getMinAmount2Number() {
        return minAmount2Number;
    }

    /**
     * Largo:8
     *
     * @param minAmount2Number the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_MIN_AMOUNT2
     *
     * @mbggenerated
     */
    public void setMinAmount2Number(Integer minAmount2Number) {
        this.minAmount2Number = minAmount2Number;
    }

    /**
     * Largo:8
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_MIN_AMOUNT1
     *
     * @mbggenerated
     */
    public Integer getMinAmount1Number() {
        return minAmount1Number;
    }

    /**
     * Largo:8
     *
     * @param minAmount1Number the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_NMR_MIN_AMOUNT1
     *
     * @mbggenerated
     */
    public void setMinAmount1Number(Integer minAmount1Number) {
        this.minAmount1Number = minAmount1Number;
    }

    /**
     * Largo:3
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_PRM_CURRENCY2
     *
     * @mbggenerated
     */
    public String getCurrency2Param() {
        return currency2Param;
    }

    /**
     * Largo:3
     *
     * @param currency2Param the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_PRM_CURRENCY2
     *
     * @mbggenerated
     */
    public void setCurrency2Param(String currency2Param) {
        this.currency2Param = currency2Param == null ? null : currency2Param.trim();
    }

    /**
     * Largo:3
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_PRM_CURRENCY1
     *
     * @mbggenerated
     */
    public String getCurrency1Param() {
        return currency1Param;
    }

    /**
     * Largo:3
     *
     * @param currency1Param the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_PRM_CURRENCY1
     *
     * @mbggenerated
     */
    public void setCurrency1Param(String currency1Param) {
        this.currency1Param = currency1Param == null ? null : currency1Param.trim();
    }

    /**
     * Largo:1
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_FLG_KMS
     *
     * @mbggenerated
     */
    public String getKmsFlg() {
        return kmsFlg;
    }

    /**
     * Largo:1
     *
     * @param kmsFlg the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_FLG_KMS
     *
     * @mbggenerated
     */
    public void setKmsFlg(String kmsFlg) {
        this.kmsFlg = kmsFlg == null ? null : kmsFlg.trim();
    }

    
//    public void setKms(boolean kms)
//    {
//    	if(kms){
//    		setKmsFlg("Y");
//    	} else {
//    		setKmsFlg("N");
//    	}
//    }
//    
//    public boolean isKms(){
//    	return "Y".equals(getKmsFlg());
//    }
    public void setKmsOrigin(boolean kms){
    	if(kms)
    		setCurrency1Param(KMS);
    	else
    		setCurrency1Param("");
    }

    public void setKmsDest(boolean kms){
    	if(kms)
    		setCurrency2Param(KMS);
    	else
    		setCurrency2Param("");
    }
    
    
    
    public boolean isKmsOrigin(){
    	return KMS.equals(getCurrency1Param());
    }

    public boolean isKmsDest(){
    	return KMS.equals(getCurrency2Param());
    }

    /**
     * Largo:7
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_FCH_MODIF
     *
     * @mbggenerated
     */
    public Date getModifDate() {
        return modifDate;
    }

    /**
     * Largo:7
     *
     * @param modifDate the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_FCH_MODIF
     *
     * @mbggenerated
     */
    public void setModifDate(Date modifDate) {
        this.modifDate = modifDate;
    }

    /**
     * Largo:7
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_FCH_CREATED
     *
     * @mbggenerated
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * Largo:7
     *
     * @param createdDate the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_FCH_CREATED
     *
     * @mbggenerated
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * Largo:20
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_DEST_REGION
     *
     * @mbggenerated
     */
    public String getDestRegionValue() {
        return destRegionValue;
    }

    /**
     * Largo:20
     *
     * @param destRegionValue the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_DEST_REGION
     *
     * @mbggenerated
     */
    public void setDestRegionValue(String destRegionValue) {
        this.destRegionValue = destRegionValue == null ? null : destRegionValue.trim();
    }

    /**
     * Largo:20
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_DEST_COUNTRY
     *
     * @mbggenerated
     */
    public String getDestCountryValue() {
        return destCountryValue;
    }

    /**
     * Largo:20
     *
     * @param destCountryValue the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_DEST_COUNTRY
     *
     * @mbggenerated
     */
    public void setDestCountryValue(String destCountryValue) {
        this.destCountryValue = destCountryValue == null ? null : destCountryValue.trim();
    }

    /**
     * Largo:20
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_ORIGIN_REGION
     *
     * @mbggenerated
     */
    public String getOriginRegionValue() {
        return originRegionValue;
    }

    /**
     * Largo:20
     *
     * @param originRegionValue the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_ORIGIN_REGION
     *
     * @mbggenerated
     */
    public void setOriginRegionValue(String originRegionValue) {
        this.originRegionValue = originRegionValue == null ? null : originRegionValue.trim();
    }

    /**
     * Largo:20
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_ORIGIN_COUNTRY
     *
     * @mbggenerated
     */
    public String getOriginCountryValue() {
        return originCountryValue;
    }

    /**
     * Largo:20
     *
     * @param originCountryValue the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_ORIGIN_COUNTRY
     *
     * @mbggenerated
     */
    public void setOriginCountryValue(String originCountryValue) {
        this.originCountryValue = originCountryValue == null ? null : originCountryValue.trim();
    }

    /**
     * Largo:1
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_POS
     *
     * @mbggenerated
     */
    public String getPosValue() {
        return posValue;
    }

    /**
     * Largo:1
     *
     * @param posValue the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_VLR_POS
     *
     * @mbggenerated
     */
    public void setPosValue(String posValue) {
        this.posValue = posValue == null ? null : posValue.trim();
    }

    /**
     * Largo:1
     *
     * @return the value of EXSPARTA.VALID_COMBINATION_OD.VCOMOD_FLG_ACTIVE
     *
     * @mbggenerated
     */
    public String getActiveFlg() {
        return activeFlg;
    }

    /**
     * Largo:1
     *
     * @param activeFlg the value for EXSPARTA.VALID_COMBINATION_OD.VCOMOD_FLG_ACTIVE
     *
     * @mbggenerated
     */
    public void setActiveFlg(String activeFlg) {
        this.activeFlg = activeFlg == null ? null : activeFlg.trim();
    }

    /**
     * This method corresponds to the database table EXSPARTA.VALID_COMBINATION_OD
     *
     * @mbggenerated
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ValidCombinationOd other = (ValidCombinationOd) that;
        return (this.getOriginValue() == null ? other.getOriginValue() == null : this.getOriginValue().equals(other.getOriginValue()))
            && (this.getDestValue() == null ? other.getDestValue() == null : this.getDestValue().equals(other.getDestValue()));
    }

    /**
     * This method corresponds to the database table EXSPARTA.VALID_COMBINATION_OD
     *
     * @mbggenerated
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getOriginValue() == null) ? 0 : getOriginValue().hashCode());
        result = prime * result + ((getDestValue() == null) ? 0 : getDestValue().hashCode());
        return result;
    }

    /**
     * This method corresponds to the database table EXSPARTA.VALID_COMBINATION_OD
     *
     * @mbggenerated
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(1024);//(128);
        sb.append(getClass().getName())
        
        .append(Integer.toHexString(System.identityHashCode(this)))
        .append('[')
        .append(super.toString())
        .append(",originValue=").append(originValue)
        .append(",destValue=").append(destValue)
        .append(",cxrParam=").append(cxrParam)
        .append(",cabinParam=").append(cabinParam)
        .append(",marketValue=").append(marketValue)
        .append(",routingNumber=").append(routingNumber)
        .append(",tarifPrivate2Number=").append(tarifPrivate2Number)
        .append(",tarifPrivate1Number=").append(tarifPrivate1Number)
        .append(",tarifViaPaNumber=").append(tarifViaPaNumber)
        .append(",tarifViaAtNumber=").append(tarifViaAtNumber)
        .append(",surchargeValue=").append(surchargeValue)
        .append(",maxAmount2Number=").append(maxAmount2Number)
        .append(",maxAmount1Number=").append(maxAmount1Number)
        .append(",minAmount2Number=").append(minAmount2Number)
        .append(",minAmount1Number=").append(minAmount1Number)
        .append(",currency2Param=").append(currency2Param)
        .append(",currency1Param=").append(currency1Param)
        .append(",kmsFlg=").append(kmsFlg)
        .append(",modifDate=").append(modifDate)
        .append(",createdDate=").append(createdDate)
        .append(",destRegionValue=").append(destRegionValue)
        .append(",destCountryValue=").append(destCountryValue)
        .append(",originRegionValue=").append(originRegionValue)
        .append(",originCountryValue=").append(originCountryValue)
        .append(",posValue=").append(posValue)
        .append(",activeFlg=").append(activeFlg)
        .append(",serialVersionUID=").append(serialVersionUID)
        .append(']');
        return sb.toString();
    }


	public List<String> getCxrParamList() {
		if(cxrParam==null){
			return Collections.emptyList();
		}
		return Arrays.asList(cxrParam.split(","))  ;
	}

	public void setCxrParamList(List<String> cxrParamList) {
		this.cxrParam = StringUtils.join(cxrParamList, ","); 
	}
    
	


	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getWarningMessage() {
		return warningMessage;
	}

	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}
	
	
}