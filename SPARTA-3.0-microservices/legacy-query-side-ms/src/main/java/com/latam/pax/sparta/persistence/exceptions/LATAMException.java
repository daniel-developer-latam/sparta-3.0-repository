package com.latam.pax.sparta.persistence.exceptions;


public class LATAMException extends Exception {

	private static final long serialVersionUID = 1L;

    public LATAMException() {
        
    }

    /**
 * @param message Mensage.
 */
    public LATAMException(String message) {
        super(message);
    }

    /**
 * @param cause Causa.
 */
    public LATAMException(Throwable cause) {
        super(cause);
    }

    /**
 * @param message Mensage.
 * @param cause Causa.
 */
    public LATAMException(String message, Throwable cause) {
        super(message, cause);
    }
}