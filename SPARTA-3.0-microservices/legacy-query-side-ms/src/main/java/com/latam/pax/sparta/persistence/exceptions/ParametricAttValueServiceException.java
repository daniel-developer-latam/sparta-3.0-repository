package com.latam.pax.sparta.persistence.exceptions;

/**
 * Esta clase fue generada por Generador de codigo de TCS. No se recomienda modificar manualmente.
 * @author Jonathan Loyola <jonathan.loyola@tcs.com>
 * 
 * 
 */
public class ParametricAttValueServiceException extends Exception {
    /** * 
*/
    private static final long serialVersionUID = 1L;

    public ParametricAttValueServiceException() {
        
    }

    /**
 * @param message Mensage.
 */
    public ParametricAttValueServiceException(String message) {
        super(message);
    }

    /**
 * @param cause Causa.
 */
    public ParametricAttValueServiceException(Throwable cause) {
        super(cause);
    }

    /**
 * @param message Mensage.
 * @param cause Causa.
 */
    public ParametricAttValueServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}