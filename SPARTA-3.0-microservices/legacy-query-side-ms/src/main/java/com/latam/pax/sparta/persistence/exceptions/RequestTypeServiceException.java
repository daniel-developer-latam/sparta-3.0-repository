package com.latam.pax.sparta.persistence.exceptions;

/**
 * Esta clase fue generada por Generador de codigo de TCS. No se recomienda modificar manualmente.
 * @author Jonathan Loyola <jonathan.loyola@tcs.com>
 * 
 * 
 */
public class RequestTypeServiceException extends Exception {
    /** * 
*/
    private static final long serialVersionUID = 1L;

    public RequestTypeServiceException() {
        
    }

    /**
 * @param message Mensage.
 */
    public RequestTypeServiceException(String message) {
        super(message);
    }

    /**
 * @param cause Causa.
 */
    public RequestTypeServiceException(Throwable cause) {
        super(cause);
    }

    /**
 * @param message Mensage.
 * @param cause Causa.
 */
    public RequestTypeServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}