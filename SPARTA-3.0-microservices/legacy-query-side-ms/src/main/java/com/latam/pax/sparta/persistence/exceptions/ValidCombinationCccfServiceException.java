package com.latam.pax.sparta.persistence.exceptions;

/**
 * Esta clase fue generada por Generador de codigo de TCS. No se recomienda modificar manualmente.
 * @author Jonathan Loyola <jonathan.loyola@tcs.com>
 * 
 * 
 */
public class ValidCombinationCccfServiceException extends Exception {
    /** * 
*/
    private static final long serialVersionUID = 1L;

    public ValidCombinationCccfServiceException() {
        
    }

    /**
 * @param message Mensage.
 */
    public ValidCombinationCccfServiceException(String message) {
        super(message);
    }

    /**
 * @param cause Causa.
 */
    public ValidCombinationCccfServiceException(Throwable cause) {
        super(cause);
    }

    /**
 * @param message Mensage.
 * @param cause Causa.
 */
    public ValidCombinationCccfServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}