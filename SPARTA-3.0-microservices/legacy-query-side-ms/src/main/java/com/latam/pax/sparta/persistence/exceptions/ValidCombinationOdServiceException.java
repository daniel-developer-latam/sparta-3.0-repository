package com.latam.pax.sparta.persistence.exceptions;

/**
 * Esta clase fue generada por Generador de codigo de TCS. No se recomienda modificar manualmente.
 * @author Jonathan Loyola <jonathan.loyola@tcs.com>
 * 
 * 
 */
public class ValidCombinationOdServiceException extends Exception {
    /** * 
*/
    private static final long serialVersionUID = 1L;

    public ValidCombinationOdServiceException() {
        
    }

    /**
 * @param message Mensage.
 */
    public ValidCombinationOdServiceException(String message) {
        super(message);
    }

    /**
 * @param cause Causa.
 */
    public ValidCombinationOdServiceException(Throwable cause) {
        super(cause);
    }

    /**
 * @param message Mensage.
 * @param cause Causa.
 */
    public ValidCombinationOdServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}