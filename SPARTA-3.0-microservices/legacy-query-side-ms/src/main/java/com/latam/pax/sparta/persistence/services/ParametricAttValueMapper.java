package com.latam.pax.sparta.persistence.services;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.latam.pax.sparta.persistence.domain.ParametricAttValue;
import com.latam.pax.sparta.persistence.exceptions.ParametricAttValueServiceException;

@Mapper
public interface ParametricAttValueMapper {
	public static final String RQTYPE_ID = "rqtypeId";
	public static final String TECONF_ID = "teconfId";
	public static final String KEY_VALUE = "keyValue";
	public static final String PAATTY_ID = "paattyId";

	
	ParametricAttValue selectByPrimaryKey(@Param(PAATTY_ID) String paattyId,
			@Param(KEY_VALUE) String keyValue, @Param(TECONF_ID) String teconfId)
			throws ParametricAttValueServiceException;

	List<ParametricAttValue> selectByTypeAndConfig(
			@Param(PAATTY_ID) String paattyId, @Param(TECONF_ID) String teconfId)
			throws ParametricAttValueServiceException;

	List<ParametricAttValue> selectByType(@Param(PAATTY_ID) String paattyId)
			throws ParametricAttValueServiceException;	

}