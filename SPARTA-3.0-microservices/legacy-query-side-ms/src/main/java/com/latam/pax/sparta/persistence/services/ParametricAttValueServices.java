package com.latam.pax.sparta.persistence.services;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.latam.pax.sparta.persistence.domain.ParametricAttValue;
import com.latam.pax.sparta.persistence.exceptions.ParametricAttValueServiceException;
import com.latam.rm.sparta.flows.exception.LegacyException;

import lombok.extern.log4j.Log4j;

/**
 * Esta clase fue generada por Generador de codigo de TCS. 
 * Se recomienda implementar todos los metodos de esta clase. 
 *  
 * Esta clase debe implementar los metodos de la clase ParametricAttValueMapper 
 * @author Jonathan Loyola <jonathan.loyola@tcs.com>
 * 
 * 
 */
@Log4j
@Service
public class ParametricAttValueServices {
    
	@Autowired
	ParametricAttValueMapper mapper;
	
    public List<ParametricAttValue> selectByTypeAndConfig(
    		@Param("paattyId") String paattyId, @Param("teconfId") String teconfId) throws LegacyException{
    	List<ParametricAttValue> parametricAttValueList = null;    	
		try {
			parametricAttValueList = mapper.selectByTypeAndConfig(paattyId, teconfId);
		} catch (ParametricAttValueServiceException e) {
			log.error(e.getMessage(), e);

			throw new LegacyException(e.getMessage(), e);
		} catch (Exception e) {
			log.error(e.getMessage(), e);

			throw new LegacyException(e.getMessage(), e);
		}
		
		return parametricAttValueList;
    	
    }
    
    public ParametricAttValue selectByPrimaryKey(
    		String paattyId, String keyValue, String teconfId) throws LegacyException{
    	ParametricAttValue parametricAttValue = null;
    	try {
			parametricAttValue = mapper.selectByPrimaryKey(paattyId, keyValue, teconfId);
		} catch (ParametricAttValueServiceException e) {
			log.error(e.getMessage(), e);

			throw new LegacyException(e.getMessage(), e);
		} catch (Exception e) {
			log.error(e.getMessage(), e);

			throw new LegacyException(e.getMessage(), e);
		}
    	return parametricAttValue;
    }
   
    public List<ParametricAttValue> selectByType(
    		@Param("paattyId") String paattyId) throws LegacyException {
    	List<ParametricAttValue> parametricAttValueList = null;
    	try {
			parametricAttValueList = mapper.selectByType(paattyId);
		} catch (ParametricAttValueServiceException e) {
			log.error(e.getMessage(), e);

			throw new LegacyException(e.getMessage(), e);
		} catch (Exception e) {
			log.error(e.getMessage(), e);

			throw new LegacyException(e.getMessage(), e);
		}    	
    	return parametricAttValueList;
    }   
}