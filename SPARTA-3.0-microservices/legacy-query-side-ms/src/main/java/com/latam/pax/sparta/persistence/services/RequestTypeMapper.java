package com.latam.pax.sparta.persistence.services;

import com.latam.pax.sparta.persistence.domain.RequestType;
import com.latam.pax.sparta.persistence.exceptions.RequestTypeServiceException;

public interface RequestTypeMapper {
    
    /**
     * This method corresponds to the database table EXSPARTA.REQUEST_TYPES
     */
    RequestType selectByPrimaryKey(String rqtypeId) throws RequestTypeServiceException;  
    
}