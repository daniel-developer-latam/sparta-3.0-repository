package com.latam.pax.sparta.persistence.services;

import org.springframework.beans.factory.annotation.Autowired;

import com.latam.pax.sparta.persistence.domain.RequestType;
import com.latam.pax.sparta.persistence.exceptions.RequestTypeServiceException;

/**
 * Esta clase fue generada por Generador de codigo de TCS. Se recomienda
 * implementar todos los metodos de esta clase.
 * 
 * Esta clase debe implementar los metodos de la clase RequestTypeMapper
 * 
 * @author Jonathan Loyola <jonathan.loyola@tcs.com>
 * 
 * 
 */
public class RequestTypeServices {
	
	@Autowired
	RequestTypeMapper mapper;
	
	public RequestType selectByPrimaryKey(String rqtypeId)
			throws RequestTypeServiceException {
		return mapper.selectByPrimaryKey(rqtypeId);
	}	
}