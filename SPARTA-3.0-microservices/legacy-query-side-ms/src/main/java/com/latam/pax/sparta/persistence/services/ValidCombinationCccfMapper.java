package com.latam.pax.sparta.persistence.services;

import com.latam.pax.sparta.persistence.criteria.CriteriaQuery;
import com.latam.pax.sparta.persistence.exceptions.ValidCombinationCccfServiceException;

public interface ValidCombinationCccfMapper {
	  
	int countCombinationView(CriteriaQuery criteriaQuery) throws ValidCombinationCccfServiceException;
		
}