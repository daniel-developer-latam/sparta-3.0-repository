package com.latam.pax.sparta.persistence.services;

import org.springframework.beans.factory.annotation.Autowired;

import com.latam.pax.sparta.persistence.criteria.CriteriaQuery;
import com.latam.pax.sparta.persistence.exceptions.ValidCombinationCccfServiceException;

/**
 * Esta clase fue generada por Generador de codigo de TCS. 
 * Se recomienda implementar todos los metodos de esta clase. 
 *  
 * Esta clase debe implementar los metodos de la clase ValidCombinationCccfMapper 
 * @author Jonathan Loyola <jonathan.loyola@tcs.com>
 * 
 * 
 */
public class ValidCombinationCccfServices {
	
	@Autowired
	ValidCombinationCccfMapper mapper;
	
    /**
     * count de items en funcion de los filtros del criteria query
     *
     */
    public int countCombinationView(CriteriaQuery example) throws ValidCombinationCccfServiceException{
    	return mapper.countCombinationView(example);
    }
}