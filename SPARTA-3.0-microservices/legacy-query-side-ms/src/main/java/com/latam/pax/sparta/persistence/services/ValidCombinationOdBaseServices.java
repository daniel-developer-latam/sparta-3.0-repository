package com.latam.pax.sparta.persistence.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.latam.pax.sparta.persistence.domain.ValidCombinationOd;
import com.latam.pax.sparta.persistence.exceptions.ValidCombinationOdServiceException;

/**
 * Esta clase fue generada por Generador de codigo de TCS. 
 * Se recomienda implementar todos los metodos de esta clase. 
 *  
 * Esta clase debe implementar los metodos de la clase ValidCombinationOdMapper 
 * @author Jonathan Loyola <jonathan.loyola@tcs.com>
 * 
 * 
 */
public class ValidCombinationOdBaseServices {
	
	@Autowired
	ValidCombinationOdMapper mapper;

	public List<ValidCombinationOd> selectByPrimaryAndCarrier(String originValue, String destValue, String cabin, 
			String carriers) throws ValidCombinationOdServiceException{
		
		String carriersCount = String.valueOf(carriers.split(",").length);
		carriers = "(" + carriers.replace(",", ")|(") + ")";

		try {
			return mapper.selectByPrimaryAndCarrier(originValue, destValue, cabin, carriers, carriersCount);
		} catch (Exception e) {
			throw new ValidCombinationOdServiceException(e);
		}
		
	}
	
}	
