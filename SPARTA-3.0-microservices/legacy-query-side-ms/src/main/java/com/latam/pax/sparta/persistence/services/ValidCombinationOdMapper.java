package com.latam.pax.sparta.persistence.services;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.latam.pax.sparta.persistence.domain.ValidCombinationOd;
import com.latam.pax.sparta.persistence.exceptions.ValidCombinationOdServiceException;

public interface ValidCombinationOdMapper {
	public static final String DEST_VALUE = "destValue";
	public static final String ORIGIN_VALUE = "originValue";
	public static final String CABIN_VALUE = "cabinValue";
	public static final String CXR_VALUE = "carrierValue";
	public static final String CXR_COUNT = "carrierCount";
	public static final String CXR_PARAM = "cxrParam";

	List<ValidCombinationOd> selectByPrimaryAndCarrier(@Param(ORIGIN_VALUE) String originValue,
			@Param(DEST_VALUE) String destValue, @Param(CABIN_VALUE) String cabin, @Param(CXR_VALUE) String carriers,
			@Param(CXR_COUNT) String carriersCount) throws ValidCombinationOdServiceException;

}