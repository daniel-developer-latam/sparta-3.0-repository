package com.latam.rm.sparta.flows.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.latam.pax.sparta.persistence.domain.ParametricAttValue;
import com.latam.pax.sparta.persistence.services.ParametricAttValueServices;
import com.latam.rm.sparta.flows.exception.LegacyException;

@RestController
@RequestMapping("/parametricAttValues")
public class ParametricAttValueController {
	
	@Autowired
	ParametricAttValueServices service;
	
	@RequestMapping(method = RequestMethod.GET, path = "/selectByTypeAndConfig")
	public @ResponseBody List<ParametricAttValue> selectByTypeAndConfig (
			@RequestParam String paattyId, @RequestParam String teconfId) throws LegacyException {
		return service.selectByTypeAndConfig(paattyId, teconfId);				
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/selectByPrimaryKey")
	public @ResponseBody ParametricAttValue selectByPrimaryKey (
			@RequestParam String paattyId, @RequestParam String keyValue, @RequestParam String teconfId) throws LegacyException {
			return	service.selectByPrimaryKey(paattyId, keyValue, teconfId);		
	}
	

	@RequestMapping(method = RequestMethod.GET, path = "/selectByType")
	public @ResponseBody List<ParametricAttValue> selectByType (
			@RequestParam String paattyId) throws LegacyException {
			return service.selectByType(paattyId);			
	}	
}
