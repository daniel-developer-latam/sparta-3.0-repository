package com.latam.rm.robots.business;

public interface PriceQuoteRobotBO {	
	
	<T> T getPassengerNameRecordByCode(String code); 
	
}
