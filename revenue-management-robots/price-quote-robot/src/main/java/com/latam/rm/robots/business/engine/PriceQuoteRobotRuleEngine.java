package com.latam.rm.robots.business.engine;

import com.latam.rm.robots.exceptions.PriceQuoteRobotEngineException;

public abstract class PriceQuoteRobotRuleEngine {

	public PriceQuoteRobotRuleEngine following;

	public abstract void execute(Object T) throws PriceQuoteRobotEngineException;

	public void setFollowing(PriceQuoteRobotRuleEngine following) {
		this.following = following;
	}

	public void process(Object o) throws PriceQuoteRobotEngineException {
		this.execute(o);
		if (following != null) {
			this.following.process(o);
		}
	}

}
