package com.latam.rm.robots.business.engine;

import java.util.Arrays;
import java.util.List;

import com.latam.rm.robots.business.rules.BusinessRule;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PriceQuoteRobotRuleFactory {

	private static final List<Class<? extends BusinessRule>> clazzRules = Arrays.asList();

	public static PriceQuoteRobotRuleEngine createNewEngineInstance() {
		PriceQuoteRobotRuleEngine fareNext = null;
		PriceQuoteRobotRuleEngine fareValidation = null;

		try {
			for (Class clazzRule : clazzRules) {
				PriceQuoteRobotRuleEngine following = (PriceQuoteRobotRuleEngine) clazzRule.newInstance();

				if (fareValidation == null) {
					fareValidation = following;
					fareNext = fareValidation;
				} else {
					fareNext.setFollowing(following);
					fareNext = following;
				}
			}

		} catch (InstantiationException e) {
			log.error(e.getMessage(), e);

		} catch (IllegalAccessException e) {
			log.error(e.getMessage(), e);
		}

		return fareValidation;
	}
}
