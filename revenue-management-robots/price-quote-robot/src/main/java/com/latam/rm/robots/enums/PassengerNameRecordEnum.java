package com.latam.rm.robots.enums;

import java.util.HashMap;

public enum PassengerNameRecordEnum {

	GROUP_FIELD(1, "Group");

	private final Integer id;
	private final String name;

	private PassengerNameRecordEnum(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public static HashMap<Integer, String> getHashMapValues() {
		HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
		for (PassengerNameRecordEnum e : PassengerNameRecordEnum.values()) {
			hashMap.put(e.getId(), e.getName());
		}
		return hashMap;
	}
}