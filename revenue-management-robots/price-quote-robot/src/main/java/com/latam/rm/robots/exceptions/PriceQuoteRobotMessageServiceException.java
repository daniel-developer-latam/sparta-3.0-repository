package com.latam.rm.robots.exceptions;

import javax.jms.JMSException;

public class PriceQuoteRobotMessageServiceException extends JMSException {
	
	private static final long serialVersionUID = 1L;

	public PriceQuoteRobotMessageServiceException(String reason) {
		super(reason);	
	}
}
