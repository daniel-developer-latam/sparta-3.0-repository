package com.latam.rm.robots.pqms;

import javax.jms.Session;

import com.latam.rm.robots.exceptions.PriceQuoteRobotMessageServiceException;
import com.latam.rm.robots.pqms.adapters.ConnectionAdapter;

public class PriceQuoteConnection extends ConnectionAdapter {
	
	@Override
	public Session createSession() throws PriceQuoteRobotMessageServiceException {		
		return null;
	}
	
	@Override
	public void close() throws PriceQuoteRobotMessageServiceException {
		return;
	}
}
