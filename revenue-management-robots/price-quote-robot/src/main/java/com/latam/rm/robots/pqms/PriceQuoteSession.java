package com.latam.rm.robots.pqms;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;

import com.latam.rm.robots.exceptions.PriceQuoteRobotMessageServiceException;
import com.latam.rm.robots.pqms.adapters.SessionAdapter;

public class PriceQuoteSession extends SessionAdapter {
	
	@Override
	public MessageProducer createProducer(Destination destination) throws PriceQuoteRobotMessageServiceException {
		return null;	
	}
	
	
	@Override
	public MessageConsumer createConsumer(Destination destination) throws PriceQuoteRobotMessageServiceException {
		return null;	
	}
	
	@Override
	public void close() throws PriceQuoteRobotMessageServiceException {
		return;
	}
}
