package com.latam.rm.robots.pqms.adapters;

import javax.jms.Connection;
import javax.jms.ConnectionConsumer;
import javax.jms.ConnectionMetaData;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.ServerSessionPool;
import javax.jms.Session;
import javax.jms.Topic;

public abstract class ConnectionAdapter implements Connection {

	public Session createSession(boolean transacted, int acknowledgeMode) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public Session createSession(int sessionMode) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public Session createSession() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public String getClientID() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public void setClientID(String clientID) throws JMSException {
		throw new UnsupportedOperationException();		
	}

	public ConnectionMetaData getMetaData() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public ExceptionListener getExceptionListener() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public void setExceptionListener(ExceptionListener listener) throws JMSException {
		throw new UnsupportedOperationException();		
	}

	public void start() throws JMSException {
		throw new UnsupportedOperationException();		
	}

	public void stop() throws JMSException {
		throw new UnsupportedOperationException();		
	}

	public void close() throws JMSException {
		throw new UnsupportedOperationException();		
	}

	public ConnectionConsumer createConnectionConsumer(Destination destination, String messageSelector,
			ServerSessionPool sessionPool, int maxMessages) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public ConnectionConsumer createSharedConnectionConsumer(Topic topic, String subscriptionName,
			String messageSelector, ServerSessionPool sessionPool, int maxMessages) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public ConnectionConsumer createDurableConnectionConsumer(Topic topic, String subscriptionName,
			String messageSelector, ServerSessionPool sessionPool, int maxMessages) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public ConnectionConsumer createSharedDurableConnectionConsumer(Topic topic, String subscriptionName,
			String messageSelector, ServerSessionPool sessionPool, int maxMessages) throws JMSException {
		throw new UnsupportedOperationException();
	}

}
