package com.latam.rm.robots.pqms.adapters;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;

import com.latam.rm.robots.exceptions.PriceQuoteRobotMessageServiceException;

public abstract class ConnectionFactoryAdapter implements ConnectionFactory {

	public Connection createConnection() throws PriceQuoteRobotMessageServiceException {
		throw new UnsupportedOperationException();
	}

	public Connection createConnection(String userName, String password) throws PriceQuoteRobotMessageServiceException {
		throw new UnsupportedOperationException();
	}
	
	public JMSContext createContext() {
		throw new UnsupportedOperationException();
	}

	public JMSContext createContext(String userName, String password) {		
		throw new UnsupportedOperationException();
	}

	public JMSContext createContext(String userName, String password, int sessionMode) {		
		throw new UnsupportedOperationException();
	}

	public JMSContext createContext(int sessionMode) {		
		throw new UnsupportedOperationException();
	}

}
