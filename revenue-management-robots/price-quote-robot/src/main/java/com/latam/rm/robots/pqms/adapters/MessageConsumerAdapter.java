package com.latam.rm.robots.pqms.adapters;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;

public class MessageConsumerAdapter implements MessageConsumer {

	public String getMessageSelector() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public MessageListener getMessageListener() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public void setMessageListener(MessageListener listener) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public Message receive() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public Message receive(long timeout) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public Message receiveNoWait() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public void close() throws JMSException {
		throw new UnsupportedOperationException();
	}

}
