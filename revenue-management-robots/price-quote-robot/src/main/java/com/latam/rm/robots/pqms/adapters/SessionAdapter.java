package com.latam.rm.robots.pqms.adapters;

import java.io.Serializable;

import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.StreamMessage;
import javax.jms.TemporaryQueue;
import javax.jms.TemporaryTopic;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;

public abstract class SessionAdapter implements Session  {

	public BytesMessage createBytesMessage() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public MapMessage createMapMessage() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public Message createMessage() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public ObjectMessage createObjectMessage() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public ObjectMessage createObjectMessage(Serializable object) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public StreamMessage createStreamMessage() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public TextMessage createTextMessage() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public TextMessage createTextMessage(String text) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public boolean getTransacted() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public int getAcknowledgeMode() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public void commit() throws JMSException {
		throw new UnsupportedOperationException();		
	}

	public void rollback() throws JMSException {
		throw new UnsupportedOperationException();		
	}

	public void close() throws JMSException {
		throw new UnsupportedOperationException();		
	}

	public void recover() throws JMSException {		
		throw new UnsupportedOperationException();
	}

	public MessageListener getMessageListener() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public void setMessageListener(MessageListener listener) throws JMSException {
		throw new UnsupportedOperationException();
		
	}

	public void run() {
		throw new UnsupportedOperationException();		
	}

	public MessageProducer createProducer(Destination destination) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public MessageConsumer createConsumer(Destination destination) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public MessageConsumer createConsumer(Destination destination, String messageSelector) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public MessageConsumer createConsumer(Destination destination, String messageSelector, boolean noLocal)
			throws JMSException {
		throw new UnsupportedOperationException();
	}

	public MessageConsumer createSharedConsumer(Topic topic, String sharedSubscriptionName) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public MessageConsumer createSharedConsumer(Topic topic, String sharedSubscriptionName, String messageSelector)
			throws JMSException {
		throw new UnsupportedOperationException();
	}

	public Queue createQueue(String queueName) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public Topic createTopic(String topicName) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public TopicSubscriber createDurableSubscriber(Topic topic, String name) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public TopicSubscriber createDurableSubscriber(Topic topic, String name, String messageSelector, boolean noLocal)
			throws JMSException {
		throw new UnsupportedOperationException();
	}

	public MessageConsumer createDurableConsumer(Topic topic, String name) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public MessageConsumer createDurableConsumer(Topic topic, String name, String messageSelector, boolean noLocal)
			throws JMSException {
		throw new UnsupportedOperationException();
	}

	public MessageConsumer createSharedDurableConsumer(Topic topic, String name) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public MessageConsumer createSharedDurableConsumer(Topic topic, String name, String messageSelector)
			throws JMSException {
		throw new UnsupportedOperationException();
	}

	public QueueBrowser createBrowser(Queue queue) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public QueueBrowser createBrowser(Queue queue, String messageSelector) throws JMSException {
		throw new UnsupportedOperationException();
	}

	public TemporaryQueue createTemporaryQueue() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public TemporaryTopic createTemporaryTopic() throws JMSException {
		throw new UnsupportedOperationException();
	}

	public void unsubscribe(String name) throws JMSException {
		throw new UnsupportedOperationException();		
	}

}
