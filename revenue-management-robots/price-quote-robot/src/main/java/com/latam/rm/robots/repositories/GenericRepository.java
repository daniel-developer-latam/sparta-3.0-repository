package com.latam.rm.robots.repositories;

public interface GenericRepository {
	
	<T> T getPassengerNameRecordByCode(String code); 

}
