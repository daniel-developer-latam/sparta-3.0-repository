package com.latam.rm.robots.utils;

public interface PassengerNameRecordValidator {

	<T> boolean checkIfExistsPassengerNameRecord(T PassengerNameRecord);
	
	<T> boolean checkIfExistsGroupFieldInPassengerNameRecord(T PassengerNameRecord);
	
	<T> boolean checkIfAllSegmentsConfirmedInPassengerNameRecord(T PassengerNameRecord);
}
